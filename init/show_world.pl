#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-initscript-show_world.pl');

my $json = readFile($WDIR."/worldedit.json");
my $w = JSON::decode_json($json);
my $a = $w->{'attributes'};
my @messages = (
$GREEN.$BOLD.'Chrons: '.$RESET.$w->{'chrons'},
$GREEN.$BOLD.'World tilt: '.$RESET.$a->{'world_tilt'},
$YELLOW.$BOLD.'Geometry:         '.$GREEN.$BOLD.'Type '.$RESET.$w->{'geometry'}->{'type'}.$GREEN.$BOLD.' Size: '.$RESET.$w->{'geometry'}->{'size'},
$YELLOW.$BOLD.'Altitude:         '.$BLUE.$BOLD.'Minimum: '.$RESET.$w->{'altitude'}->{'minimum'}.'m '.$BLUE.$BOLD.' Maximum: '.$RESET.$w->{'altitude'}->{'maximum'}.'m',
$YELLOW.$BOLD.'Salinity:         '.$BLUE.$BOLD.'Minimum: '.$RESET.$w->{'salinity'}->{'minimum'}.'% '.$BLUE.$BOLD.' Maximum: '.$RESET.$w->{'salinity'}->{'maximum'}.'%',
$YELLOW.$BOLD.'Soil humidity:    '.$BLUE.$BOLD.'Minimum: '.$RESET.$w->{'soil_humidity'}->{'minimum'}.'% '.$BLUE.$BOLD.' Maximum: '.$RESET.$w->{'soil_humidity'}->{'maximum'}.'%',
$YELLOW.$BOLD.'Atm. humidity:    '.$MAGENTA.$BOLD.'Default: '.$RESET.$w->{'atm_humidity'}->{'default'},
$YELLOW.$BOLD.'Temperature:      '.$BLUE.$BOLD.'Minimum: '.$RESET.$w->{'temperature'}->{'minimum'}.'C '.$BLUE.$BOLD.' Maximum: '.$RESET.$w->{'temperature'}->{'maximum'}.'C'.$BLUE.$BOLD.' Max. gradient: '.$RESET.$w->{'temperature'}->{'maximum_gradient'},
$YELLOW.$BOLD.'                  '.$BLUE.$BOLD.'Percentage: '.$RESET.$w->{'temperature'}->{'percentage'}.'% '.$BLUE.$BOLD.' Randomness: '.$RESET.$w->{'temperature'}->{'randomness'},
$YELLOW.$BOLD.'Pressure:         '.$BLUE.$BOLD.'Minimum: '.$RESET.$w->{'pressure'}->{'minimum'}.' '.$BLUE.$BOLD.' Maximum: '.$RESET.$w->{'pressure'}->{'maximum'}.$BLUE.$BOLD.' Max. gradient: '.$RESET.$w->{'pressure'}->{'maximum_gradient'},
$YELLOW.$BOLD.'                  '.$BLUE.$BOLD.'Percentage: '.$RESET.$w->{'pressure'}->{'percentage'}.'% '.$BLUE.$BOLD.' Randomness: '.$RESET.$w->{'pressure'}->{'randomness'}.$MAGENTA.$BOLD.' Default: '.$RESET.$w->{'pressure'}->{'default'},
$YELLOW.$BOLD.'Land percentage:  '.$RESET.$w->{'land_percentage'}.'%',
$YELLOW.$BOLD.'Accidents:        '.$GREEN.$BOLD.'Strength: '.$RESET.$w->{'accidents'}->{'strength'},
$YELLOW.$BOLD.'Erosion::         '.$GREEN.$BOLD.'Strength: '.$RESET.$w->{'erosion'}->{'strength'}.' '.$GREEN.$BOLD.' Percentage: '.$RESET.$w->{'erosion'}->{'percentage'}.'%'.$GREEN.$BOLD.' Randomness: '.$RESET.$w->{'erosion'}->{'randomness'},
$YELLOW.$BOLD.'Collapse:         '.$GREEN.$BOLD.'Strength: '.$RESET.$w->{'collapse'}->{'strength'}.' '.$GREEN.$BOLD.' Percentage: '.$RESET.$w->{'collapse'}->{'percentage'}.'%'.$GREEN.$BOLD.' Randomness: '.$RESET.$w->{'collapse'}->{'randomness'},
$YELLOW.$BOLD.'Waterfall:        '.$GREEN.$BOLD.'Percentage: '.$RESET.$w->{'waterfall'}->{'percentage'}.'%'.$GREEN.$BOLD.' Randomness: '.$RESET.$w->{'waterfall'}->{'randomness'},
$GREEN.$BOLD.'Year: '.$RESET.$a->{'year'}
.$GREEN.$BOLD.' Ticks-per-day: '.$RESET.$a->{'ticks_per_day'}
.$GREEN.$BOLD.' Days-per-year: '.$RESET.$a->{'days_per_year'},
''
);
callSend($INPUT->{'player'}, 'plain', dclone(\@messages));

1;
