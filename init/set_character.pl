#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-initscript-set_character.pl');

my $attribute = $INPUT->{'arguments'}[0];
my $value = $INPUT->{'arguments'}[1];

sub is_race {
  my $arg = shift;
  my $is_race = dbQueryAtom("select id from races where id = '".$arg."';");
  if ($is_race) {
    return 1;
  } else {
    return 0;
  }
}

sub is_gender {
  my $arg = shift;
  my $is_gender = dbQueryAtom("select id from genders where id = '".$arg."';");
  if ($is_gender) {
    return 1;
  } else {
    return 0;
  }
}

sub is_character {
  my $arg = shift;
  my $is_character = dbQueryAtom("select id from characters where id = '".$arg."';");
  if ($is_character) {
    return 1;
  } else {
    return 0;
  }
}

if ($attribute eq 'race') {
  if (is_race($value)) {
    dbExecute("update players set race = '".$value."' where username = '".$player."';");
    callSend($INPUT->{'player'}, 'plain', $BOLD.$BLUE.'Your race is now '.$YELLOW.$BOLD.$value);
  } else {
    callSend($INPUT->{'player'}, 'error', $value.' is not a race');
  }
} elsif ($attribute eq 'gender') {
  if (is_gender($value)) {
    dbExecute("update players set gender = '".$value."' where username = '".$player."';");
    callSend($INPUT->{'player'}, 'plain', $BOLD.$BLUE.'Your gender is now '.$YELLOW.$BOLD.$value);
  } else {
    callSend($INPUT->{'player'}, 'error', $value.' is not a gender');
  }
} elsif ($attribute eq 'class') {
  if (is_character($value)) {
    dbExecute("update players set class = '".$value."' where username = '".$player."';");
    callSend($INPUT->{'player'}, 'plain', $BOLD.$BLUE.'Your class is now '.$YELLOW.$BOLD.$value);
  } else {
    callSend($INPUT->{'player'}, 'error', $value.' is not a class');
  }
} elsif ($attribute eq 'name') {
  dbExecute("update players set name = '".$value."' where username = '".$player."';");
  callSend($INPUT->{'player'}, 'plain', $BOLD.$BLUE.'Your name is now '.$YELLOW.$BOLD.$value);
} else {
  callSend($INPUT->{'player'}, 'error', $attribute.': invalid attribute');
}

1;
