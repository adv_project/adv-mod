#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-initscript-launch_character.pl');

my $player = $INPUT->{'player'};
my $race = dbQueryAtom("select p.race from players p where p.username = '".$player."';");
my $gender = dbQueryAtom("select p.gender from players p where p.username = '".$player."';");
my $class = dbQueryAtom("select p.class from players p where p.username = '".$player."';");
my $name = dbQueryAtom("select p.name from players p where p.username = '".$player."';");
my $fail;

unless ($race) {
  $race = dbQueryAtom("
  select id from races
  order by random()
  limit 1
  ;");
  dbExecute("update players set race = '".$race."' where username = '".$player."';");
#  callSend($player, 'warning', "You didn't pick a race");
#  $fail = 1;
}
unless ($gender) {
  $gender = dbQueryAtom("
  select id from genders
  order by random()
  limit 1
  ;");
  dbExecute("update players set gender = '".$gender."' where username = '".$player."';");
#  callSend($player, 'warning', "You didn't pick a gender");
#  $fail = 1;
}
unless ($class) {
  $class = dbQueryAtom("
  select id from characters
  order by random()
  limit 1
  ;");
  dbExecute("update players set class = '".$class."' where username = '".$player."';");
#  callSend($player, 'warning', "You didn't pick a class");
#  $fail = 1;
}
unless ($name) {
  $name = $player;
  dbExecute("update players set name = '".$player."' where username = '".$player."';");
}
if ($fail) { exit 1; }
# Write the database
# TODO: wrap this in a single pg function
#TODO: this should me launched by a single flag inside the db
my $parent;
#my $try = 0;

do {
  $try = $try + 1;
  $parent = dbQueryAtom("
  select o.id from objects o, tiles t where
  o.id = t.id and
  abs(t.latitude) < 45.0 and
  'landmass' = any(array(select get_all_tags(o.id)))
  order by id
  limit 1
  ;");
} until (($parent) or ($try > 50));

my $json = '{
	"parent":"'.$parent.'",
	"class":"'.$class.'",
	"gender":"'.$gender.'",
	"race":"'.$race.'",
	"is_avatar":"yes",
	"init":"yes"
}';
my $created_id = dbQueryAtom("select * from register_new_object('".$json."'::jsonb, true);");
dbExecute("update players set avatar = '".$created_id."', name = '".$name."', classes = array['".$gender."','".$race."','".$class."'] where username = '".$player."';");
# TODO: locate the object somewhere

setPlayerConnected($player);
setPlayerPhase($player, 'game');
writeFile($WDIR.'/error.json', qw<[]>);
bannerEnterGame($player);

my @welcome = (
$CYAN."You are ".$name.", a ".$race." ".$class.". ".$BOLD."Look".$RESET.$CYAN.", if you want to see."
);
callSend($player, 'plain', dclone(\@welcome));

1
