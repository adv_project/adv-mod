#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-initscript-show_world.pl');

my $worldpath = $WDIR."/worldedit.json";
my $json = readFile($worldpath);
my $w = JSON::decode_json($json);

my $attribute = $INPUT->{'arguments'}[0];
my $value = $INPUT->{'arguments'}[1];

if ($attribute eq 'chrons') {
  $value = $value * 1;
  $w->{'chrons'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Chrons: '.$CYAN.$value);
} elsif ($attribute eq 'wtilt') {
  $value = $value * 1;
  $w->{'attributes'}->{'world_tilt'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Geometry: '.$GREEN.'Size '.$CYAN.$value);
} elsif ($attribute eq 'gtype') {
  $w->{'geometry'}->{'type'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Geometry: '.$GREEN.'Type '.$CYAN.$value);
} elsif ($attribute eq 'gsize') {
  $value = $value * 1;
  $w->{'geometry'}->{'size'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Geometry: '.$GREEN.'Size '.$CYAN.$value);

} elsif ($attribute eq 'altmin') {
  $value = $value * 1.0;
  $w->{'altitude'}->{'minimum'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Altitude: '.$BLUE.'Minimum: '.$CYAN.$value);
} elsif ($attribute eq 'altmax') {
  $value = $value * 1.0;
  $w->{'altitude'}->{'maximum'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Altitude: '.$BLUE.'Maximum: '.$CYAN.$value);

} elsif ($attribute eq 'salmin') {
  $value = $value * 1.0;
  $w->{'salinity'}->{'minimum'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Salinity: '.$BLUE.'Minimum: '.$CYAN.$value);
} elsif ($attribute eq 'salmax') {
  $value = $value * 1.0;
  $w->{'salinity'}->{'maximum'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Salinity: '.$BLUE.'Maximum: '.$CYAN.$value);

} elsif ($attribute eq 'shummin') {
  $value = $value * 1.0;
  $w->{'soil_humidity'}->{'minimum'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Soil humidity: '.$BLUE.'Minimum: '.$CYAN.$value);
} elsif ($attribute eq 'shummax') {
  $value = $value * 1.0;
  $w->{'soil_humidity'}->{'maximum'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Soil humidity: '.$BLUE.'Maximum: '.$CYAN.$value);

} elsif ($attribute eq 'ahummin') {
  $value = $value * 1.0;
  $w->{'atm_humidity'}->{'minimum'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Atm. humidity: '.$BLUE.'Minimum: '.$CYAN.$value);
} elsif ($attribute eq 'ahummax') {
  $value = $value * 1.0;
  $w->{'atm_humidity'}->{'maximum'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Atm. humidity: '.$BLUE.'Maximum: '.$CYAN.$value);

} elsif ($attribute eq 'tempmin') {
  $value = $value * 1.0;
  $w->{'temperature'}->{'minimum'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Temperature: '.$BLUE.'Minimum: '.$CYAN.$value);
} elsif ($attribute eq 'tempmax') {
  $value = $value * 1.0;
  $w->{'temperature'}->{'maximum'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Temperature: '.$BLUE.'Maximum: '.$CYAN.$value);
} elsif ($attribute eq 'tempmaxgrad') {
  $value = $value * 1.0;
  $w->{'temperature'}->{'maximum_gradient'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Temperature: '.$BLUE.'Max. gradient: '.$CYAN.$value);
} elsif ($attribute eq 'tempperc') {
  $value = $value * 1.0;
  $w->{'temperature'}->{'percentage'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Temperature: '.$BLUE.'Percentage: '.$CYAN.$value);
} elsif ($attribute eq 'temprnd') {
  $value = $value * 1.0;
  $w->{'temperature'}->{'randomness'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Temperature: '.$BLUE. 'Randomness: '.$CYAN.$value);
} elsif ($attribute eq 'tempdef') {
  $value = $value * 1.0;
  $w->{'temperature'}->{'default'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Temperature: '.$BLUE. 'Default: '.$CYAN.$value);

} elsif ($attribute eq 'pressmin') {
  $value = $value * 1.0;
  $w->{'pressure'}->{'minimum'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Pressure: '.$BLUE.'Minimum: '.$CYAN.$value);
} elsif ($attribute eq 'pressmax') {
  $value = $value * 1.0;
  $w->{'pressure'}->{'maximum'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Pressure: '.$BLUE.'Maximum: '.$CYAN.$value);
} elsif ($attribute eq 'pressmaxgrad') {
  $value = $value * 1.0;
  $w->{'pressure'}->{'maximum_gradient'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Pressure: '.$BLUE.'Max. gradient: '.$CYAN.$value);
} elsif ($attribute eq 'pressperc') {
  $value = $value * 1.0;
  $w->{'pressure'}->{'percentage'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Pressure: '.$BLUE.'Percentage: '.$CYAN.$value);
} elsif ($attribute eq 'pressrnd') {
  $value = $value * 1.0;
  $w->{'pressure'}->{'randomness'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Pressure: '.$BLUE. 'Randomness: '.$CYAN.$value);
} elsif ($attribute eq 'pressdef') {
  $value = $value * 1.0;
  $w->{'pressure'}->{'default'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Pressure: '.$BLUE. 'Default: '.$CYAN.$value);

} elsif ($attribute eq 'landprc') {
  $value = $value * 1.0;
  $w->{'land_percentage'}->{'default'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Land percentage: '.$CYAN.$value);

} elsif ($attribute eq 'accstr') {
  $value = $value * 1.0;
  $w->{'accidents'}->{'strength'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Accidents: '.$BLUE. 'Strength: '.$CYAN.$value);

} elsif ($attribute eq 'erostr') {
  $value = $value * 1.0;
  $w->{'erosion'}->{'strength'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Erosion: '.$BLUE. 'Strength: '.$CYAN.$value);
} elsif ($attribute eq 'eroperc') {
  $value = $value * 1.0;
  $w->{'erosion'}->{'percentage'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Erosion: '.$BLUE. 'Percentage: '.$CYAN.$value);
} elsif ($attribute eq 'erornd') {
  $value = $value * 1.0;
  $w->{'erosion'}->{'randomness'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Erosion: '.$BLUE. 'Randomness: '.$CYAN.$value);

} elsif ($attribute eq 'colstr') {
  $value = $value * 1.0;
  $w->{'collapse'}->{'strength'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Collapse: '.$BLUE. 'Strength: '.$CYAN.$value);
} elsif ($attribute eq 'colperc') {
  $value = $value * 1.0;
  $w->{'collapse'}->{'percentage'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Collapse: '.$BLUE. 'Percentage: '.$CYAN.$value);
} elsif ($attribute eq 'colrnd') {
  $value = $value * 1.0;
  $w->{'collapse'}->{'randomness'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Collapse: '.$BLUE. 'Randomness: '.$CYAN.$value);

} elsif ($attribute eq 'watperc') {
  $value = $value * 1.0;
  $w->{'waterfall'}->{'percentage'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Waterfall: '.$BLUE. 'Percentage: '.$CYAN.$value);
} elsif ($attribute eq 'watrnd') {
  $value = $value * 1.0;
  $w->{'waterfall'}->{'randomness'} = $value;
  writeFile($worldpath, JSON->new->pretty(1)->encode($w));
  callSend($INPUT->{'player'}, 'plain', $YELLOW.$BOLD.'Waterfall: '.$BLUE. 'Randomness: '.$CYAN.$value);

} else {
  callSend($INPUT->{'player'}, 'error', $attribute.': invalid attribute');
}

# Draft from here
#my @messages = (
#$YELLOW.$BOLD.'Waterfall:        '.$GREEN.$BOLD.'Percentage: '.$RESET.$a->{'waterfall'}->{'percentage'}.'%'.$GREEN.$BOLD.' Randomness: '.$RESET.$a->{'waterfall'}->{'randomness'},
#''
#);

1;
