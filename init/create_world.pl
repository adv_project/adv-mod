#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";

local $/;
my $tag = dbQueryAtom('select new_id(4);');
my $awgpath = $WDIR . "/mods/adv/init/launch_awg.pl";
my $player = $GAMEPROPERTIES->{'owner'};
callSendTagged($player, "plain", "Creating world...", $tag);
callRun($awgpath, {});
my $importpath = $WDIR . "/mods/adv/init/import_objects.pl";
callSendTagged($player, "plain", "Importing mod files to database...", $tag);
callRun($importpath, {});
my $initpath = $WDIR . "/mods/adv/init/populate_world.pl";
callSendTagged($player, "plain", "Populating world...", $tag);
callRun($initpath, {});
callSendTagged($player, "plain", "World successfully created", $tag);
bannerCreateCharacter($player);

# Launch the character sequence
setPlayerPhase($player, 'character');

1;
