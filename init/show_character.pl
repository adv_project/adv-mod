#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-initscript-show_character.pl');

my $player = $INPUT->{'player'};
my @messages = ();

# Name
my $name = dbQueryAtom("select name from players where username = '".$player."';");

if ($name) {
  push(@messages, $BLUE.$BOLD.'Your name is '.$YELLOW.$BOLD.$name);
} else {
  push(@messages, $BLUE.$BOLD.'Your name is '.$MAGENTA.$BOLD.$player);
}

push(@messages, '');

# Race
my $race = dbQueryAtom("select race from players where username = '".$player."';");

if ($race) {
  push(@messages, $BLUE.$BOLD.'  Race:    '.$GREEN.$BOLD.$race),
} else {
  push(@messages, $BLUE.$BOLD.'  Race:    '.$MAGENTA.$BOLD.join(', ', @{ dbQuerySingle('select id from races;') })),
}

# Gender
my $gender = dbQueryAtom("select gender from players where username = '".$player."';");

if ($gender) {
  push(@messages, $BLUE.$BOLD.'  Gender:  '.$GREEN.$BOLD.$gender),
} else {
  push(@messages, $BLUE.$BOLD.'  Gender:  '.$MAGENTA.$BOLD.join(', ', @{ dbQuerySingle('select id from genders;') })),
}

# Class
my $class = dbQueryAtom("select class from players where username = '".$player."';");

if ($class) {
  push(@messages, $BLUE.$BOLD.'  Class:   '.$GREEN.$BOLD.$class),
} else {
  push(@messages, $BLUE.$BOLD.'  Class:   '.$MAGENTA.$BOLD.join(', ', @{ dbQuerySingle('select id from characters;') })),
}

push(@messages, '');

if (($race) and ($gender) and ($class)) {
  my $attrjson = dbQueryAtom("select estimate_init_attributes('".$player."');");
  my $a = JSON::decode_json($attrjson);
  push(@messages, $YELLOW.$BOLD.'Strength: '.$CYAN.$BOLD.$a->{'strength'});
  push(@messages, $YELLOW.$BOLD.'Constitution: '.$CYAN.$BOLD.$a->{'constitution'});
  push(@messages, $YELLOW.$BOLD.'Speed: '.$CYAN.$BOLD.$a->{'speed'});
  push(@messages, $YELLOW.$BOLD.'Intelligence: '.$CYAN.$BOLD.$a->{'intelligence'});
  push(@messages, $YELLOW.$BOLD.'Dexterity: '.$CYAN.$BOLD.$a->{'dexterity'});
  push(@messages, $YELLOW.$BOLD.'Stealth: '.$CYAN.$BOLD.$a->{'stealth'});
  push(@messages, $YELLOW.$BOLD.'Bravery: '.$CYAN.$BOLD.$a->{'bravery'});
  push(@messages, $YELLOW.$BOLD.'Wisdom: '.$CYAN.$BOLD.$a->{'wisdom'});
  push(@messages, $YELLOW.$BOLD.'Charisma: '.$CYAN.$BOLD.$a->{'charisma'});
  push(@messages, $YELLOW.$BOLD.'Dominance: '.$CYAN.$BOLD.$a->{'dominance'});
  push(@messages, $YELLOW.$BOLD.'Self_preservation: '.$CYAN.$BOLD.$a->{'self_preservation'});
  push(@messages, $YELLOW.$BOLD.'Curiosity: '.$CYAN.$BOLD.$a->{'curiosity'});
  push(@messages, '');
}

callSend($INPUT->{'player'}, 'plain', dclone(\@messages));

1;
