#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-initscript-import_objects.pl');

# Import objects
my $database = 'adv';
my $userid = 'adv';
my $schema = $GAMEPROPERTIES->{'name'};
my $retval;
$retval = system($WDIR.'/mods/jsonlib/bin/import_objects "dbname='.$database.' host='.$DBHOST.' user='.$userid.'" '.$schema.' '.$WDIR.' '.$WDIR.'/awg.json');
logMsg(5, "system(".$WDIR.'/mods/jsonlib/bin/import_objects "dbname='.$database.' host='.$DBHOST.' user='.$userid.'" '.$schema.' '.$WDIR.' '.$WDIR.'/awg.json) => '.$retval);

1;
