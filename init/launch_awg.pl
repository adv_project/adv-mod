#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-initscript-launch_awg.pl');

# Import objects
my $awgpath = $WDIR.'/mods/awg/bin/awg';
my $awginputfile = $WDIR . "/worldedit.json";
my $retval;
print "system($awgpath, $WDIR, $awginputfile)\n";
$retval = system($awgpath, $WDIR, $awginputfile, $awgmetapath);
logMsg(5, "Launch awg => ".$retval);

1;
