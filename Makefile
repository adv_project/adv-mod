SHELL := /bin/bash
ifndef PREFIX
  PREFIX = /usr
endif
MOD := adv
VERSION=$(shell git describe | rev | cut -d- -f2- | rev | sed -e 's/-/.r/' -e 's/^v//')

default:

.PHONY: clean

.FORCE:

install:
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/assets
	for asset in  $(wildcard assets/*) ; do \
		install -Dv -m 644 $$asset $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$asset ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/autoload
	for pl in  $(wildcard autoload/*) ; do \
		install -Dv -m 644 $$pl $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$pl ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/sql.d
	for sql in  $(wildcard sql.d/*) ; do \
		install -Dv -m 644 $$sql $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$sql ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/words
	for word in  $(wildcard words/*) ; do \
		install -Dv -m 644 $$word $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$word ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/dictionary
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/dictionary/en
	for word in  $(wildcard dictionary/en/*) ; do \
		install -Dv $$word $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$word ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/commands
	for command in  $(wildcard commands/*) ; do \
		install -Dv -m 644 $$command $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$command ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/scripts
	for script in  $(wildcard scripts/*) ; do \
		install -Dv -m 755 $$script $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$script ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/init
	for initf in  $(wildcard init/*) ; do \
		install -Dv -m 755 $$initf $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$initf ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/hooks
	for hook in  $(wildcard hooks/*) ; do \
		install -Dv -m 755 $$hook $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$hook ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json
	install -Dv -m 644 json/index.json $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json/index.json
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json/ecoregions
	for ecoregion in  $(wildcard json/ecoregions/*) ; do \
		install -Dv -m 644 $$ecoregion $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$ecoregion ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json/biomes
	for biome in  $(wildcard json/biomes/*) ; do \
		install -Dv -m 644 $$biome $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$biome ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json/proto
	for proto in  $(wildcard json/proto/*) ; do \
		install -Dv -m 644 $$proto $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$proto ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json/characters
	for character in  $(wildcard json/characters/*) ; do \
		install -Dv -m 644 $$character $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$character ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json/classes
	for class in  $(wildcard json/classes/*) ; do \
		install -Dv -m 644 $$class $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$class ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json/elements
	for element in  $(wildcard json/elements/*) ; do \
		install -Dv -m 644 $$element $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$element ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json/genders
	for gender in  $(wildcard json/genders/*) ; do \
		install -Dv -m 644 $$gender $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$gender ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json/populations
	for population in  $(wildcard json/populations/*) ; do \
		install -Dv -m 644 $$population $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$population ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json/races
	for race in  $(wildcard json/races/*) ; do \
		install -Dv -m 644 $$race $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$race ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json/meta
	for metaobj in  $(wildcard json/meta/*) ; do \
		install -Dv -m 644 $$metaobj $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$metaobj ; \
	done
	@mkdir -p $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/json/knowledge
	for knowledgeobj in  $(wildcard json/knowledge/*) ; do \
		install -Dv -m 644 $$knowledgeobj $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/$$knowledgeobj ; \
	done
	install -v -m 644 properties.json $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/
	@sed -i $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)/properties.json -e "s/[\"]version[\"]: [\"].*[\"]/\"version\": \"$(VERSION)\"/"

uninstall:
	@rm -r $(DESTDIR)/$(PREFIX)/share/adv/mods/$(MOD)

clean:

