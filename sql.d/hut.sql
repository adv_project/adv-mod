CREATE OR REPLACE FUNCTION on_init_hut(
  object_id varchar
)
RETURNS VOID AS $$
	BEGIN
		RAISE NOTICE 'Initializing a hut.';
	END;
$$ language plpgsql;

CREATE OR REPLACE FUNCTION on_destroy_hut(
  object_id varchar
)
RETURNS VOID AS $$
	BEGIN
		RAISE NOTICE 'A hut was destroyed!';
	END;
$$ language plpgsql;

