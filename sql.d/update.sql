/**
 * ``run_update()`` -- per-tick update routine
**/
CREATE OR REPLACE FUNCTION run_update()
RETURNS INTEGER AS $$
  
  DECLARE
    this_world RECORD;
  
  BEGIN
/* Clean attribute values estimated previously */
    DELETE FROM estimated_values;

/*
 * Tier 0 in this design is the world itself, visit all of them
 */
    FOR this_world IN
      SELECT  id
      FROM    objects
      WHERE   'world' = any(classes)
    LOOP
/*
 * Run world events, one by one
 */
      PERFORM advance_time(this_world.id);
      PERFORM update_active_tiles(this_world.id);
    END LOOP;

    RETURN 0;
  END;
$$ language plpgsql;

