CREATE OR REPLACE FUNCTION calculate_sun_height(
  tile VARCHAR
)
RETURNS NUMERIC AS $$
  
  DECLARE
    stats         RECORD;
    latitude      NUMERIC;
    longitude     NUMERIC;
    time          NUMERIC;
    season        NUMERIC;
    declination   NUMERIC;
    max_height    NUMERIC;
    min_height    NUMERIC;
    sun_offset    NUMERIC;
    sun_height    NUMERIC;

  BEGIN
    SELECT get_attribute_value(
      tile,
      'latitude'
    )
    INTO latitude;

    SELECT get_attribute_value(
      tile,
      'longitude'
    )
    INTO longitude;

    SELECT  w.tick_of_day,
            w.day_of_year,
            w.ticks_per_day,
            w.days_per_year,
            w.world_tilt
    FROM    worlds w,
            links l
    WHERE   l.target = w.id
    AND     l.type = 'parent'
    AND     l.id = tile
    INTO    stats;

    time := (
      stats.tick_of_day::NUMERIC
      +
      stats.day_of_year::NUMERIC*stats.ticks_per_day::NUMERIC
    ) / stats.ticks_per_day::NUMERIC + longitude/180.0;

    season      := sin(2*pi() * time / stats.days_per_year::NUMERIC);
    declination := stats.world_tilt * season / (abs(latitude)/60 + 1);
    max_height  := 90.0 - latitude + declination;
    min_height  := -90.0 + latitude + declination;

    IF max_height > 90.0 THEN
      max_height := 180.0 - max_height;
    END IF;

    IF min_height < -90.0 THEN
      min_height := -180.0 - min_height;
    END IF;

    sun_offset := (min_height + max_height)*0.5;
    sun_height := sun_offset + (max_height - sun_offset) * sin(2*pi() * time);

    RETURN sun_height;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_moon_height(
  tile VARCHAR
)
RETURNS NUMERIC AS $$
  
  DECLARE
    stats         RECORD;
    latitude      NUMERIC;
    longitude     NUMERIC;
    time          NUMERIC;
    season        NUMERIC;
    declination   NUMERIC;
    max_height    NUMERIC;
    min_height    NUMERIC;
    moon_offset   NUMERIC;
    moon_height   NUMERIC;
    moon_tilt     NUMERIC;

  BEGIN
    SELECT get_attribute_value(
      tile,
      'latitude'
    )
    INTO latitude;

    SELECT get_attribute_value(
      tile,
      'longitude'
    )
    INTO longitude;

    SELECT  w.tick_of_day,
            w.day_of_year,
            w.ticks_per_day,
            w.days_per_year
    FROM    worlds w,
            links l
    WHERE   l.target = w.id
    AND     l.type = 'parent'
    AND     l.id = tile
    INTO    stats;

    /*TODO: MIGRATE to json */
    moon_tilt := 5.0;

    time := (
      stats.tick_of_day::NUMERIC
      +
      stats.day_of_year::NUMERIC*stats.ticks_per_day::NUMERIC
    ) / stats.ticks_per_day::NUMERIC + longitude/180.0;

    season      := sin(2*pi() * 12*time / stats.days_per_year::NUMERIC);
    declination := moon_tilt * season;
    max_height  := 90.0 - latitude + declination;
    min_height  := -90.0 + latitude + declination;

    IF max_height > 90.0 THEN
      max_height := 180.0 - max_height;
    END IF;

    IF min_height < -90.0 THEN
      min_height := -180.0 - min_height;
    END IF;

    moon_offset := (min_height + max_height)*0.5;
    moon_height := moon_offset + (max_height - moon_offset) * sin(2*pi() * (28/27)*time);

    RETURN moon_height;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_moon_phase(
  tile VARCHAR
)
RETURNS NUMERIC AS $$
  
  DECLARE
    stats         RECORD;
    longitude     NUMERIC;
    time          NUMERIC;
    moon_phase    NUMERIC;

  BEGIN
    SELECT get_attribute_value(
      tile,
      'longitude'
    )
    INTO longitude;

    SELECT  w.tick_of_day,
            w.day_of_year,
            w.ticks_per_day,
            w.days_per_year
    FROM    worlds w,
            links l
    WHERE   l.target = w.id
    AND     l.type = 'parent'
    AND     l.id = tile
    INTO    stats;

    time := (
      stats.tick_of_day::NUMERIC
      +
      stats.day_of_year::NUMERIC*stats.ticks_per_day::NUMERIC
    ) / stats.ticks_per_day::NUMERIC + longitude/180.0;

    moon_phase := sin(2*pi() * 12*time / stats.days_per_year::NUMERIC);

    RETURN moon_phase;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_east_wind(
  object_id VARCHAR
)
RETURNS NUMERIC AS $$
  BEGIN
    RETURN 42.0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_east_wind(
  object_id VARCHAR
)
RETURNS NUMERIC AS $$
  BEGIN
    RETURN 42.0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_north_wind(
  object_id VARCHAR
)
RETURNS NUMERIC AS $$
  BEGIN
    RETURN 42.0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_high_clouds(
  object_id VARCHAR
)
RETURNS NUMERIC AS $$
  BEGIN
    RETURN 42.0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_rain(
  object_id VARCHAR
)
RETURNS NUMERIC AS $$
  BEGIN
    RETURN 42.0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_atm_temperature(
  object_id VARCHAR
)
RETURNS NUMERIC AS $$
  BEGIN
    RETURN 42.0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_atm_pressure(
  object_id VARCHAR
)
RETURNS NUMERIC AS $$
  BEGIN
    RETURN 42.0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_atm_humidity(
  object_id VARCHAR
)
RETURNS NUMERIC AS $$
  BEGIN
    RETURN 42.0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_landmass_index(
  object_id VARCHAR
)
RETURNS INTEGER AS $$
  
  DECLARE
    landmass_index  NUMERIC;
    o               VARCHAR;

  BEGIN
    FOR o IN
      SELECT  target
      FROM    links
      WHERE   id = object_id
      AND     type = 'neighbour'
    LOOP
      SELECT 100.0 INTO landmass_index;
    END LOOP;

    RETURN 0.42;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION sun_height_to_visibility(
  sun_height  NUMERIC
)
RETURNS NUMERIC AS $$
  
  DECLARE
    brightness  NUMERIC;
    
  BEGIN
    --TODO: detect altitude gradient to west and east
    IF  sun_height >= 23.0 THEN
      RETURN 100.0;
    ELSIF  sun_height < -15.0 THEN
      RETURN 0.0;
    ELSE
      IF  sun_height < 5.0 THEN
        brightness = 70;
      ELSIF  sun_height > 12.0 THEN
        brightness = 100.0;
      ELSE
        brightness = 55;
      END IF;

      RETURN
        brightness * (15+sun_height) / 37;
    END IF;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION calculate_visibility(
  object_id VARCHAR
)
RETURNS NUMERIC AS $$
  
  DECLARE
    parent_link           RECORD;
    parent_slot           RECORD;
    inherited_visibility  NUMERIC;
    coef                  NUMERIC;
    sun_height            NUMERIC;
    
  BEGIN
    /* The parent object */
    SELECT  l.name,
            l.target
    FROM    links l
    WHERE   l.id = object_id
    AND     l.type = 'parent'
    INTO    parent_link;

    /* The parent slot */
    SELECT  *
    FROM    object_parent_slot(object_id)
    INTO    parent_slot;
    
    /* Check how far we are from direct sunlight */
    IF  parent_slot.name != 'world'
    AND parent_slot.closure <> 100.0
    THEN
      inherited_visibility := estimate_attribute(parent_link.target, 'visibility');
    ELSIF parent_slot.closure = 100.0
    THEN
      inherited_visibility := 0.0;
    ELSE
      sun_height := calculate_sun_height(object_id);
      inherited_visibility := sun_height_to_visibility(sun_height);
    END IF;
    /* Apply closure */
    coef := @ (parent_slot.closure - 100.0) / 100;
    RETURN inherited_visibility * coef;
  END;
$$ LANGUAGE plpgsql;

/*
        'sun_height',
        'east_wind',
        'north_wind',
        --'low_clouds',
        'high_clouds',
        'rain',
        --'snow',
        'atm_temperature',
        --'body_temperature',
        'atm_pressure',
        'atm_humidity',
        --'body_humidity',
        'landmass_index'
*/
