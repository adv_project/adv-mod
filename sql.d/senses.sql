/* Associates attribute -- listen_* as called from modify_attribute() --
 * with the desired senses
 * TODO: this function should be GENERIC
 */
CREATE OR REPLACE FUNCTION listen_sun_height(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    l     VARCHAR;
    label VARCHAR;
    i     INTEGER;
    buf   jsonb;
  
  BEGIN
    PERFORM log_event(  'debug',
                        'listen_sun_height',
                        'running event'
    );
/* Input already carries the sensitive objects -- this comes from the condition */
    IF  input ? 'it_looks'
    AND input->'it_looks' != '[]'
    THEN
      SELECT json_array_length( to_json(input->'boundaries') ) - 1
      INTO i;

      label := concat(
        'sun_moves_',
        input->>'string',
        '_',
        input->'direction'->>0,
        '_',
        input->'boundaries'->>i
      );

      FOR l IN
        SELECT *
        FROM   json_array_elements_text( to_json(input->'it_looks') )
      LOOP
/* TODO discriminate automata and connected avatars */
        buf := input || jsonb_build_object(
          'recipient'::text,      l::text,
          'message_class'::text,  'plain'::text,
          'message_label'::text,  label::text
        );
        
        PERFORM log_event(  'debug',
                            'deliver message',
                            'running event'
        );

        PERFORM push_api_call(
          'test_deliver',
          buf
        );
      END LOOP;
    END IF;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION listen_moon_height(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    l     VARCHAR;
    label VARCHAR;
    i     INTEGER;
    buf   jsonb;
    tile VARCHAR;
    sun_height numeric;
  
  BEGIN
    PERFORM log_event(  'debug',
                        'listen_moon_height',
                        'running event'
    );
/* TODO: use visibility here */
    IF  input ? 'it_looks'
    AND input->'it_looks' != '[]'
    THEN
      SELECT json_array_length( to_json(input->'boundaries') ) - 1
      INTO i;

      label := concat(
        'moon_moves_',
        input->>'string',
        '_',
        input->'direction'->>0,
        '_',
        input->'boundaries'->>i
      );

      FOR l IN
        SELECT *
        FROM   json_array_elements_text( to_json(input->'it_looks') )
      LOOP
        SELECT target FROM links WHERE id = l AND type = 'parent'
        INTO tile;

        sun_height := calculate_sun_height(tile);

        IF sun_height < 12
        THEN
/* TODO discriminate automata and connected avatars */
          buf := input || jsonb_build_object(
            'recipient'::text,      l::text,
            'message_class'::text,  'plain'::text,
            'message_label'::text,  label::text
          );
          
          PERFORM log_event(  'debug',
                              'deliver message',
                              'running event'
          );

          PERFORM push_api_call(
            'test_deliver',
            buf
          );
        END IF;
      END LOOP;
    END IF;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION listen_moon_phase(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    l     VARCHAR;
    label VARCHAR;
    i     INTEGER;
    buf   jsonb;
  
  BEGIN
    PERFORM log_event(  'debug',
                        'listen_moon_phase',
                        'running event'
    );
/* Input already carries the sensitive objects -- this comes from the condition */
    IF  input ? 'it_looks'
    AND input->'it_looks' != '[]'
    THEN
      SELECT json_array_length( to_json(input->'boundaries') ) - 1
      INTO i;

      label := concat(
        'moon_phases_',
        input->>'string',
        '_',
        input->'direction'->>0,
        '_',
        input->'boundaries'->>i
      );

      FOR l IN
        SELECT *
        FROM   json_array_elements_text( to_json(input->'it_looks') )
      LOOP
/* TODO discriminate automata and connected avatars */
        buf := input || jsonb_build_object(
          'recipient'::text,      l::text,
          'message_class'::text,  'plain'::text,
          'message_label'::text,  label::text
        );
        
        PERFORM log_event(  'debug',
                            'deliver message',
                            'running event'
        );

        PERFORM push_api_call(
          'test_deliver',
          buf
        );
      END LOOP;
    END IF;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION listen_noise(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    l     VARCHAR;
    label VARCHAR;
    i     INTEGER;
    buf   jsonb;
  
  BEGIN
    PERFORM log_event(  'debug',
                        'listen_noise',
                        'running event'
    );
/* Input already carries the sensitive objects -- this comes from the condition */
    IF  input ? 'it_hears'
    AND input->'it_hears' != '[]'
    THEN
      SELECT json_array_length( to_json(input->'boundaries') ) - 1
      INTO i;

      label := concat(
        'noise_',
        input->>'string',
        '_',
        input->'direction'->>0,
        '_',
        input->'boundaries'->>i
      );

      FOR l IN
        SELECT *
        FROM   json_array_elements_text( to_json(input->'it_hears') )
      LOOP
/* TODO discriminate automata and connected avatars */
        buf := input || jsonb_build_object(
          'recipient'::text,      l::text,
          'message_class'::text,  'plain'::text,
          'message_label'::text,  label::text
        );
        
        PERFORM log_event(  'debug',
                            'deliver message',
                            'running event'
        );

        PERFORM push_api_call(
          'test_deliver',
          buf
        );
      END LOOP;
    END IF;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION listen_learning_points(
  input jsonb
)
RETURNS INTEGER AS $$
  
  DECLARE
    l     VARCHAR;
    label VARCHAR;
    i     INTEGER;
    buf   jsonb;
  
  BEGIN
    PERFORM log_event(  'debug',
                        'listen_learning_points',
                        'running event'
    );
/* Input already carries the sensitive objects -- this comes from the condition */
    IF  input ? 'it_learns'
    AND input->'it_learns' != '[]'
    THEN
      SELECT json_array_length( to_json(input->'boundaries') ) - 1
      INTO i;

      label := concat(
        'learning_',
        input->>'string',
        '_',
        input->'direction'->>0,
        '_',
        input->'boundaries'->>i
      );

    PERFORM log_event(  'debug',
                        'listen_learning_points',
      concat(           'label: ', label  )
    );
      FOR l IN
        SELECT *
        FROM   jsonb_array_elements_text(input->'it_learns')
      LOOP
/* TODO discriminate automata and connected avatars */
        buf := input || jsonb_build_object(
          'recipient'::text,      l::text,
          'message_class'::text,  'plain'::text,
          'message_label'::text,  label::text
        );
        
        PERFORM push_api_call(
          'test_deliver',
          buf
        );
        /* TODO: dummy learning device, so crude */
        IF input->'boundaries'->>i = 'highest'
        THEN
          UPDATE  links
          SET     name = 'knowledge'
          WHERE   id = input->>'caller'
          AND     target = l;
        END IF;
      END LOOP;
    END IF;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

