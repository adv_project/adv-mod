/**
 * ``advance_time()`` -- world event
**/
CREATE OR REPLACE FUNCTION advance_time(
  world_id VARCHAR
)
RETURNS INTEGER AS $$
  
  DECLARE
    stats RECORD;
  
  BEGIN
/*
 * Initialize world time if empty
 */
    IF NOT EXISTS (
      SELECT
      FROM  worlds
      WHERE id = world_id
    )
    THEN
/* Create the row */
      INSERT INTO worlds (
        id,
        day_of_year,
        tick_of_day,
        year
      )
      VALUES (
        world_id,
        1,
        1,
        1
      );
/* Initialize it */
      PERFORM initialize_attribute(
        world_id,
        'days_per_year'
      );
      PERFORM initialize_attribute(
        world_id,
        'steps_per_tick'
      );
      PERFORM initialize_attribute(
        world_id,
        'ticks_per_day'
      );
      PERFORM initialize_attribute(
        world_id,
        'world_tilt'
      );
    END IF;
/* Read world stats */
    SELECT  *
    FROM    worlds
    WHERE   id = world_id
    INTO    stats;
/* Calculate new time */
    IF stats.tick_of_day >= stats.ticks_per_day
    THEN
      SELECT 1
      INTO stats.tick_of_day;
      
      IF stats.day_of_year >= stats.days_per_year
      THEN
        SELECT 1
        INTO stats.day_of_year;

        SELECT stats.year + 1
        INTO stats.year;
      ELSE
        SELECT stats.day_of_year + 1
        INTO stats.day_of_year;
      END IF;
    ELSE
      SELECT stats.tick_of_day + 1
      INTO stats.tick_of_day;
    END IF;
/* Update table */
    UPDATE  worlds
    SET     year = stats.year,
            day_of_year = stats.day_of_year,
            tick_of_day = stats.tick_of_day
    WHERE   id = world_id;

    RETURN 0;
  END;
$$ language plpgsql;

