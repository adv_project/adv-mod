CREATE OR REPLACE FUNCTION estimate_character_attributes(object_id VARCHAR)
RETURNS JSONB AS $$
  
  DECLARE
    output    JSONB;
  
  BEGIN
    output := jsonb_build_object(
      'strength'::TEXT,           estimate_attribute(object_id, 'strength'),
      'constitution'::TEXT,       estimate_attribute(object_id, 'constitution'),
      'speed'::TEXT,              estimate_attribute(object_id, 'speed'),
      'intelligence'::TEXT,       estimate_attribute(object_id, 'intelligence'),
      'dexterity'::TEXT,          estimate_attribute(object_id, 'dexterity'),
      'stealth'::TEXT,            estimate_attribute(object_id, 'stealth'),
      'bravery'::TEXT,            estimate_attribute(object_id, 'bravery'),
      'wisdom'::TEXT,             estimate_attribute(object_id, 'wisdom'),
      'charisma'::TEXT,           estimate_attribute(object_id, 'charisma'),
      'dominance'::TEXT,          estimate_attribute(object_id, 'dominance'),
      'self_preservation'::TEXT,  estimate_attribute(object_id, 'self_preservation'),
      'curiosity'::TEXT,          estimate_attribute(object_id, 'curiosity')
    );

    RETURN output;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION estimate_init_attributes(p VARCHAR)
RETURNS JSONB AS $$
  
  DECLARE
    temp_id   VARCHAR;
    r         VARCHAR;
    g         VARCHAR;
    c         VARCHAR;
    output    JSONB;
  
  BEGIN
    SELECT  *
    FROM    make()
    INTO    temp_id;

    SELECT  race
    FROM    players
    WHERE   username = p
    INTO    r;

    SELECT  gender
    FROM    players
    WHERE   username = p
    INTO    g;

    SELECT  class
    FROM    players
    WHERE   username = p
    INTO    c;

    UPDATE  everything
    SET     type = 'temporary',
            in_table = 'objects'
    WHERE   id = temp_id;

    INSERT INTO objects (
      id, classes, active
    )
    VALUES (
      temp_id, array[r,g,c], FALSE
    );

    output := estimate_character_attributes(temp_id);

    DELETE FROM everything WHERE id = temp_id;

    RETURN output;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_ecoregion_landmass(
  eco_id  VARCHAR
) RETURNS VOID AS $$
  DECLARE
    tile_id   VARCHAR;
  BEGIN
    perform log_event('debug', 'Populating landmass', eco_id);
    FOR tile_id IN
      SELECT l.id FROM links l
      WHERE l.target = eco_id
      AND l.type = 'ecoregion'
      AND l.name = 'border'
    LOOP
      if not exists (select id from links where id = tile_id and target = 'coast' and type = 'inherits' and name = 'shore_slot')
      then
        insert into links (id, target, type, name)
          values (tile_id, 'coast', 'inherits', 'shore_slot');
      end if;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION populate_world()
RETURNS VOID AS $$
  
  DECLARE
    eco_id      VARCHAR;
    inherited   VARCHAR[];
    i varchar;
  
  BEGIN
    FOR eco_id IN
      SELECT  e.id
      FROM    objects o,
              ecoregions e
      WHERE   e.id = o.id
    LOOP
      for i in select distinct get_all_inherited(eco_id) loop
        if is_routine_name( concat('populate_ecoregion_', i) ) then
          EXECUTE 'select populate_ecoregion_' || quote_ident(i) ||
            '(' || quote_literal(eco_id) || ');';
        end if;
      end loop;
    END LOOP;
  END;
$$ LANGUAGE plpgsql;
