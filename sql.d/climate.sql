CREATE OR REPLACE FUNCTION update_objects_recursive(
  tile_id VARCHAR
)
RETURNS INTEGER AS $$
  
  DECLARE
    children  VARCHAR[];
    object_id VARCHAR;
    trigg     RECORD;
    buffer jsonb;
    this_update jsonb;
  
  BEGIN
    SELECT all_in_self_recursive(tile_id) INTO children;

    FOREACH object_id IN ARRAY children
    LOOP
      FOR trigg IN
        SELECT *
        FROM   triggers t
        WHERE  t.target = any(array(select get_all_inherited(object_id)))
        AND    'on_update'::VARCHAR = any(t.tags)
      LOOP
        buffer := jsonb_build_object(
          'caller'::TEXT,   object_id::TEXT,
          'target'::TEXT,   array[object_id]::TEXT[]
        );
        SELECT run_trigger
        FROM   run_trigger(
          trigg.id,
          buffer
        )
        INTO   this_update;
        
        perform log_event('debug', 'Running update on object', jsonb_pretty(this_update));
        perform make_phenomena(this_update);
      
      END LOOP;
    END LOOP;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

/**
 * Launches climate events in all active objects in ``world_id``.
**/
CREATE OR REPLACE FUNCTION update_active_tiles(
  world_id VARCHAR
)
RETURNS INTEGER AS $$
  
  DECLARE
    tile VARCHAR;
  
  BEGIN
    FOR tile IN
      SELECT  l.id
      FROM    links l,
              objects o
      WHERE   o.active
      AND     o.id = l.id
      AND     l.type = 'parent'
      AND     l.target = world_id
    LOOP
      PERFORM sun_moves_in_tile(tile);
      PERFORM moon_moves_in_tile(tile);
      PERFORM update_objects_recursive(tile);
    END LOOP;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION initialize_climate(
  object_id VARCHAR
)
RETURNS INTEGER AS $$
  
  DECLARE
    tile  VARCHAR;
    value NUMERIC;
    attr  VARCHAR;

  BEGIN
    /* Just to make sure we talk about a tile */
    SELECT get_parent_tile(object_id)
    INTO tile;

    FOREACH attr IN ARRAY
      array[
        'sun_height',
        'moon_height',
        'moon_phase'
        --'east_wind',
        --'north_wind',
        --'low_clouds',
        --'high_clouds',
        --'rain',
        --'snow',
        --'atm_temperature',
        --'body_temperature',
        --'atm_pressure',
        --'atm_humidity',
        --'body_humidity',
        --'landmass_index'
      ]
    LOOP
      EXECUTE format(
        'SELECT calculate_%I(%L);',
        attr,
        tile
      )
      INTO value;

      EXECUTE format('update tiles set %I = $1 WHERE id = %L;',
      attr,
      tile
      )
      USING value;
    END LOOP;

    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

