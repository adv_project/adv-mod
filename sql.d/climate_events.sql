/* Tile event */
CREATE OR REPLACE FUNCTION sun_moves_in_tile(
  tile VARCHAR
)
RETURNS VOID AS $$
  
  DECLARE
    sun_height NUMERIC;
  
  BEGIN
    sun_height := calculate_sun_height(tile);
    
    PERFORM modify_attribute(
	 	  tile,
      'sun_height',
      'set',
      sun_height
    );
  END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION moon_moves_in_tile(
  tile VARCHAR
)
RETURNS VOID AS $$
  
  DECLARE
    moon_height NUMERIC;
    moon_phase  NUMERIC;
  
  BEGIN
    moon_height := calculate_moon_height(tile);
    moon_phase := calculate_moon_phase(tile);
    
    PERFORM modify_attribute(
	 	  tile,
      'moon_height',
      'set',
      moon_height
    );
    
    PERFORM modify_attribute(
	 	  tile,
      'moon_phase',
      'set',
      moon_phase
    );
  END;
$$ LANGUAGE plpgsql;

