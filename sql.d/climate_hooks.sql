/* Class hook */
CREATE OR REPLACE FUNCTION on_activate_climate(
  object_id VARCHAR
)
RETURNS INTEGER AS $$ 
  BEGIN
    PERFORM initialize_climate(object_id);
    RETURN 0;
  END;
$$ LANGUAGE plpgsql;

