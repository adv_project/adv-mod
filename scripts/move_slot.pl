#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-move_slot');
#FIX: move_direction

# Move in a certain direction (link->name)
my $slot = $INPUT->{'arguments'}[0];
# Who wants to change slot
my $avatar = getAvatar($player);
# The parent about to become destitute
my $parent = getParentId($avatar);
my $job = JSON->new->encode({ "child" => $avatar, "parent" => $parent, "slot_name" => $slot });
my $try = dbQueryAtom("select success from put_object_in_place('".$job."'::jsonb, true);");
if ($try) {
	my $json = '{
		"caller": "'.$avatar.'",
		"target": ["'.$avatar.'"]
	}';
	my $buf = JSON::decode_json($json);
	raise_flag('look_around', $buf);
	flush_stacks();
} else {
	my $json = '{
		"caller": "'.$avatar.'",
		"target": ["'.$avatar.'"],
		"recipient": "'.$avatar.'",
		"message_class": "plain",
		"message_label": "you_cannot_go_there"
	}';
	test_deliver($json);
}

1;
