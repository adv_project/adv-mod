#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-unalias');

# Player
my $player = $INPUT->{'player'};
my $playerfile = $WDIR."/players/".$player.".json";
local $/;
open my $pfh, '<', $playerfile;
my $json = <$pfh>;
close $pfh;
my $playerdata = JSON::decode_json($json);

my $alias = $inbuf->{'arguments'}[0]->{'verbatim'};
my $result = {};
my $flag = 0;
foreach my $k (keys(%{ $playerdata->{'aliases'}})) {
	if ($k eq $alias) {
		$flag = 1;
	} else {
		$result->{$k} = $playerdata->{'aliases'}->{$k}
	}
}

if ($flag > 0) {
	$playerdata->{'aliases'} = $result;
	my $outjson = JSON->new->pretty(1)->encode($playerdata);
	open my $pfh, '>', $playerfile;
	print $pfh $outjson;
	close $pfh;
	callSend($player, "notice", "Alias ".$alias." removed");
} else {
	callSend($player, "error", $alias." is not an alias");
}

1;
