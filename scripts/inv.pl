#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-inv');

my $self = getAvatar($player);
my $json = '{
	"caller":"'.$self.'",
	"target":["'.$self.'"]
}';
my $buffer = JSON::decode_json($json);
raise_flag('inventory', $buffer);
flush_stacks();

1;
