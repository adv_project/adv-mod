#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-sail_direction');
#FIX: move_direction

# Move in a certain direction (link->name)
my $direction = $INPUT->{'arguments'}[0];
# Who wants to change parent
my $avatar = getAvatar($player);
# The alleged vehicle
my $vehicle = getParentId($avatar);
my $is_vehicle = dbQueryAtom("select 'is_boat' = any(array(select get_all_tags('".$vehicle."')));");
if (! $is_vehicle) {
	callSend($player, "plain", "Oops! Cannot sail without a boat.");
	exit;
}
# The parent about to become destitute
my $parent = getParentId($vehicle);
# The caller pointed in a certain direction, so retrieve the target id in that direction
my $candidate = dbQueryAtom("select distinct target from links l where l.id = '".$parent."' and l.name = '".$direction."';");
if (! $candidate) {
	callSend($player, "plain", "You cannot go there.");
	exit;
}
my $job = JSON->new->encode({ "child" => $vehicle, "parent" => $candidate });
my $try = dbQueryAtom("select success from put_object_in_place('".$job."'::jsonb, true);");
if ($try) {
	my $json = '{
		"caller": "'.$avatar.'",
		"target": ["'.$avatar.'"]
	}';
	my $buf = JSON::decode_json($json);
	print "Tried, and try says $try\n";
	raise_flag('look_around', $buf);
	flush_stacks();
} else {
	my $json = '{
		"caller": "'.$avatar.'",
		"target": ["'.$avatar.'"],
		"recipient": "'.$avatar.'",
		"message_class": "plain",
		"message_label": "you_cannot_go_there"
	}';
	test_deliver($json);
}

1;
