#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-forget');

my $self = getAvatar($player);
print " . . . forgetting . . . \n";
dbExecute("delete from links where id = '".$self."' and type = 'use';");
my $json = '{
	"caller": "'.$self.'",
	"target": ["'.$self.'"],
	"recipient": "'.$self.'",
	"message_class": "plain",
	"message_label": "you_forget"
}';
test_deliver($json);

1;
