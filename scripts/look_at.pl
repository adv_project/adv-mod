#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-look_at');

my $self = getAvatar($player);
my $class = $INPUT->{'arguments'}[0];
my $json = '{
	"caller":"'.$self.'",
	"target":["'.$class.'"]
}';
my $buffer = JSON::decode_json($json);
raise_flag('analyze', $buffer);
flush_stacks();

1;
