#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-help');

my $label = $INPUT->{'arguments'}[0];
$label = 'help_index' unless ($label);

my $json = '{
	"player":"'.$player.'",
	"message_class":"help",
	"message_label":"'.$label.'"
}';
test_deliver($json);
flush_stacks();

1;
