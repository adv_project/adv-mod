#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-look');

my $self = getAvatar($player);
my $json = '{
	"caller":"'.$self.'",
	"target":["'.$self.'"]
}';
my $buffer = JSON::decode_json($json);
raise_flag('look_around', $buffer);
flush_stacks();

1;
