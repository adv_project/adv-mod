#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-stats');

my $player = $INPUT->{'player'};
my $self = getAvatar($player);
my @messages = ();

# Name
my $name = dbQueryAtom("select name from players where username = '".$player."';");
my $race = dbQueryAtom("select race from players where username = '".$player."';");
my $gender = dbQueryAtom("select gender from players where username = '".$player."';");
my $class = dbQueryAtom("select class from players where username = '".$player."';");
#TODO: translate these according to dictionary
push(@messages, $BLUE.$BOLD.'You are '.$YELLOW.$BOLD.$name.$BLUE.$BOLD.', a '.$race.' '.$gender.' '.$class);

push(@messages, '');

my $attrjson = dbQueryAtom("select estimate_character_attributes('".$self."');");
my $a = JSON::decode_json($attrjson);
push(@messages, $YELLOW.$BOLD.'Strength: '.$CYAN.$BOLD.$a->{'strength'});
push(@messages, $YELLOW.$BOLD.'Constitution: '.$CYAN.$BOLD.$a->{'constitution'});
push(@messages, $YELLOW.$BOLD.'Speed: '.$CYAN.$BOLD.$a->{'speed'});
push(@messages, $YELLOW.$BOLD.'Intelligence: '.$CYAN.$BOLD.$a->{'intelligence'});
push(@messages, $YELLOW.$BOLD.'Dexterity: '.$CYAN.$BOLD.$a->{'dexterity'});
push(@messages, $YELLOW.$BOLD.'Stealth: '.$CYAN.$BOLD.$a->{'stealth'});
push(@messages, $YELLOW.$BOLD.'Bravery: '.$CYAN.$BOLD.$a->{'bravery'});
push(@messages, $YELLOW.$BOLD.'Wisdom: '.$CYAN.$BOLD.$a->{'wisdom'});
push(@messages, $YELLOW.$BOLD.'Charisma: '.$CYAN.$BOLD.$a->{'charisma'});
push(@messages, $YELLOW.$BOLD.'Dominance: '.$CYAN.$BOLD.$a->{'dominance'});
push(@messages, $YELLOW.$BOLD.'Self_preservation: '.$CYAN.$BOLD.$a->{'self_preservation'});
push(@messages, $YELLOW.$BOLD.'Curiosity: '.$CYAN.$BOLD.$a->{'curiosity'});
push(@messages, '');

callSend($INPUT->{'player'}, 'plain', dclone(\@messages));

1;
