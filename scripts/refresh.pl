#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-lib.pl";
logInit('adv-script-refresh');

my $player = $INPUT->{'player'};
unlink $WDIR."/commands.json" if (-f $WDIR."/commands.json");
unlink $WDIR."/words.json" if (-f $WDIR."/words.json");
callSend($player, "notice", "All buffers clean");

1;
