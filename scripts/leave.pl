#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-leave');
#FIX: leave

# Leave the current parent
my $avatar = getAvatar($player);
my $parent = getParentId($avatar);
my $newparent = getParentId($parent);
if ( hasTag($parent, "biome") ) {
	# BAD. This should be a flag
	callSend( getAvatarPlayer($avatar), "plain", "you cannot go there");
	exit;
}

my $try = prepareParentLink($avatar, $newparent);
if ($try->{'result'} eq "success") {
	# TODO: flags will be a problem here, because we are no longer there to catch them
	removeParentLink($avatar);
	createParentLink($avatar, $try->{'id'}, $try->{'slot'}, 0);
	my $obj = {
		"player" => $player,
		"action" => ('look')
	};
	callRun($WDIR."/mods/adv-core/hooks/action", $obj);
} else {
	callSend($player, "plain", "there is no way there.");
	exit;
}

resolve_flags($FLAGS_FILE);

1;
