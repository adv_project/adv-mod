#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-focus');

my $self = getAvatar($player);
print " . . . focusing . . . \n";
my $arg = $INPUT->{'arguments'}[0];
my $focused = dbQueryAtom("select target from given_names_index where player = '".$player."';");
if ($arg) {
  print " .. .. .. $self is trying to make focus on $arg .. .. .. \n";
  callSend($player, "notice", "focusing on $arg");
} elsif ($focused) {
  print " .. .. .. already focused .. .. .. \n";
  callSend($player, "notice", "already focused");
} else {
  my @array = @{ dbQuerySingle("select unnest(all_available_but('".$self."'));") };
  if (@array) {
    print " .. .. .. $self is trying to make focus on @array .. .. .. \n";
    callSend($player, "notice", "focusing on @array");
  } else {
    print " .. .. .. $self is trying to make focus on nothing at all .. .. .. \n";
    callSend($player, "notice", "there is nothing to focus on");
  }
}

1;
