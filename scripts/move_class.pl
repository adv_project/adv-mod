#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-move_class');
#FIX: move_direction

# Move in a certain direction (link->name)
my $class = $INPUT->{'arguments'}[0];
# Who wants to change parent
my $avatar = getAvatar($player);
# The parent about to become destitute
my $parent = getParentId($avatar);
# The caller pointed to a certain class, see if it is a valid destination
my $candidate = dbQueryAtom("
  select p.id
  from links p, objects o
  where o.active
  and p.id = o.id
  and p.type = 'parent'
  and p.target = '".$parent."'
  and 'connected_to_parent' = any(array(select get_tags(p.id)))
  and '".$class."' = any(o.classes)
  limit 1;"
);
if (! $candidate) {
	callSend($player, "plain", "You cannot go there.");
	exit;
}
my $job = JSON->new->encode({ "child" => $avatar, "parent" => $candidate });
my $try = dbQueryAtom("select success from put_object_in_place('".$job."'::jsonb, true);");
if ($try) {
	my $json = '{
		"caller": "'.$avatar.'",
		"target": ["'.$avatar.'"]
	}';
	my $buf = JSON::decode_json($json);
	raise_flag('look_around', $buf);
	flush_stacks();
} else {
	my $json = '{
		"caller": "'.$avatar.'",
		"target": ["'.$avatar.'"],
		"recipient": "'.$avatar.'",
		"message_class": "plain",
		"message_label": "you_cannot_go_there"
	}';
	test_deliver($json);
}

1;
