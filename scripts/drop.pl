#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-drop');

my $class = $inbuf->{'arguments'}[0]->{'string'};
my $self = getAvatar($player);
my $json = '{
	"caller":"'.$self.'",
	"target":["'.$class.'"]
}';
my $buffer = JSON::decode_json($json);
raise_flag('drop', $buffer);
flush_stacks();

1;
