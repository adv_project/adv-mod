#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-alias');

# Player
my $player = $INPUT->{'player'};
my $playerfile = $WDIR."/players/".$player.".json";
local $/;
open my $pfh, '<', $playerfile;
my $json = <$pfh>;
close $pfh;
my $playerdata = JSON::decode_json($json);

if ($inbuf->{'arguments'}[0]->{'string'}) {
	my $alias = $inbuf->{'arguments'}[0]->{'string'};
	my $string = $inbuf->{'arguments'}[1]->{'string'};
	$playerdata->{'aliases'}->{$alias} = $string;
	my $outjson = JSON->new->pretty(1)->encode($playerdata);
	open my $pfh, '>', $playerfile;
	print $pfh $outjson;
	close $pfh;
	callSend($player, "notice", $alias." is now an alias for ".$string);
} else {
	my $string;
	foreach my $k (keys(%{ $playerdata->{'aliases'}})) {
		$string = $string."(".$k." ".$playerdata->{'aliases'}->{$k}.") ";
	}
	callSend($player, "notice", $string);
}

1;
