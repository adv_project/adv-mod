#!/usr/bin/env perl

require "/usr/share/adv/mods/adv-core/perl/adv-core-game.pl";
logInit('adv-script-use');

my $self = getAvatar($player);
my $class = $INPUT->{'arguments'}[0];
my $use_as = $INPUT->{'arguments'}[1];
my $json = '{
	"caller":"'.$self.'",
	"target":["'.$class.'"],
	"use_as":"'.$use_as.'"
}';
print " . . . using . . . $json\n";
my $buffer = JSON::decode_json($json);
raise_flag('use_object', $buffer);
flush_stacks();

1;
