## Motivation circuits

#### Self-maintenance

- Ingestive: hunger, thirst
- Defensive: pain, anger/agression, thermal regulation, panic/escape
- Normality/security seeking: locomotion, unknown places, fearful creatures/things

#### Self-propagation

- Social: affiliation, play, trade
- Reproductive: sexual attraction, care
- Self-development: exploration, practice, learning

### A model: _counters_

| Counters | Lowest | Low | Average | High | Highest |
|----------|--------|-----|---------|------|---------|
| integrity | death | disability | injures | normal | optimal
| thermal | death | freeze | normal | dehidration | death
| thirst | death | dehidration | normal | illness | death
| feed | death | starvation | normal | illness | death
| tiredness |  death | illness | normal | dullness | madness
| danger | madness | dullness | normal | fear | panic
| health | death | illness | dullness | normal | optimal

#### Isolated "states"

- optimal
- normal
- dull
- ill
- injured
- fearful
- panicking
- mad
- freezing
- starving
- dehidrating
- disabled
- dead

## Complex actions and phenomena

### Player actions

- move DIRECTION|PLACE  **(DISPLACE)**
- look [ANY] **(NOTICE)**
- pick THING **(DISPLACE)**
- drop THING **(DISPLACE)**
- put THING [into] THING **(DISPLACE)**
- eat **(CONSUME)**
- drink **(CONSUME)**
- read **(LEARN)**

### Automata reactions

- hide
- attack
- steal
- migrate

### World actions

- sun moves

### Interface

#### First level: complex actions

1. **DISPLACE** - Object moves from one parent/slot to another  *(unlinkFromParent, linkToParent)*
2. **NOTICE** - Object notices another object *(updateMemory)*
3. **CONSUME** - Object feeds on another object/element *(updateHealth)*

#### Second level: atomic actions

- *unlinkFromParent*
- *linkToParent*
- *updateHealth*
- *updateMemory*

#### Third level: phenomena

- *a.* Objects make contact
- *b.* Objects and environment make contact
- *c.* 
