# How to implement triggers, conditions, and flags

## Actions

### Look

Player enters the command ``look``:

```
my $buffer = {
	"caller" => $avatar
};
my $result = resolve_triggers_by_task("look_around", $avatar, $buffer, 1);
```

Avatar's race class catches the trigger:


```json
{
  "conditions": [
    {
      "arg": "TARGET",
      "amount": -1,
      "label": "unknown",
      "query_name": "all_in_parent",
      "tags": ["any"],
      "test_name": "true",
      "write": "nearby"
    },
    {
      "arg": "TARGET",
      "amount": -1,
      "label": "look_at_parent",
      "query_name": "parent",
      "tags": ["any"],
      "test_name": "true",
      "write": "parent"
    }
  ],
  "label": "looks_around",
  "tags": ["force"],
  "task": "look_around"
}
```

Which means:

```
do look_around if:
  all_in_parent(true(TARGET)) [any] as nearby
and:
  parent(true(TARGET)) [any] as parent
```

Trigger always succeedes because it has the ``force`` tag, so task is launched:

```perl
sub look_around {
	my $buffer = shift;
	my $self = $buffer->{'caller'};
	my $parent = $buffer->{'parent'}[0];
	my @message = ();
	
	@message = ("\@YOU_ARE_IN:".$parent);
	raise_flag($self, "receives_message", {"message" => dclone(\@message)}, "SCRIPT:look");
	
	my $sun_height = estimate_attribute($parent, "sun_height");
	@message = ("\@sun_height:".int($sun_height));
	raise_flag($self, "receives_message", {"message" => dclone(\@message)}, "SCRIPT:look");
	
	if (@{ $buffer->{'nearby'} }) {
		my $nearby = join(',', @{ $buffer->{'nearby'} });
		@message = ("\@YOU_SEE_NEARBY:".$nearby);
		raise_flag($self, "receives_message", {"message" => dclone(\@message)}, "SCRIPT:look");
	}
}
```

Meta class ``console`` catches the flag:


```json
{
  "conditions": [
    {
      "arg": "TARGET",
      "label": "receives_a_console_message",
      "query_name": "self",
      "test_name": "true",
      "write": "TARGET"
    }
  ],
  "label": "unknown",
      "tags": null,
      "task": "receives_message"
    },
```

Which means:


```
do receive_message if:
  self(true(TARGET)) [] as TARGET
```

Trigger succeedes, task is launched:

```perl
sub receives_message {
	my $buffer = shift;
	my $caller = $buffer->{'caller'};
	my $player = getAvatarPlayer($caller);
	my @messages = @{ $buffer->{'message'} };
	foreach my $m (0 .. (@messages - 1)) {
		callSend($player, "plain", $buffer->{'message'}[$m])
			unless (! isPlayerConnected($player));
	}
	return 1;
}

```

### Pick

Action script triggers:

```perl
my $class = $inbuf->{'arguments'}[0]->{'string'};
my $avatar = getAvatar($player);
my $buffer = {
   "caller" => $avatar
};
my $result = resolve_triggers_by_task("pick", $class, $buffer);
```

Some nearby object catches it:

```json
{
  "conditions": [
    {
      "arg": "TARGET",
      "label": "unknown",
      "query_name": "all_in_parent",
      "tags": [],
      "test_name": "has_own_class",
      "write": "TARGET"
    }
  ],
  "label": "unknown",
  "tags": [],
  "task": "pick"
},
```

Which means:

```
do pick if:
  all_in_parent(has_own_class(TARGET)) [] as TARGET
```

Task is launched:

``` perl
sub pick {
	my $buffer = shift;
	my @targets = @{ $buffer->{'target'} };
	my $newparent = $buffer->{'caller'};
	foreach my $t (0 .. (@targets - 1)) {
		my $try = prepareParentLink($targets[$t], $newparent);
		if ($try->{'result'} eq "success") {
			removeParentLink($targets[$t]);
			createParentLink($targets[$t], $try->{'id'}, $try->{'slot'}, 0);
		}
	}
	raise_flag($buffer->{'caller'}, "have_picked", { "picked" => dclone(\@targets) }, "pick");
	raise_flag($buffer->{'caller'}, "has_picked", { "picked" => dclone(\@targets) }, "pick");
}

```

Console meta class catches these flags:

```json
{
  "conditions": [
    {
      "amount": "all",
      "arg": "it_looks",
      "label": "unknown",
      "query_name": "all_in_parent",
      "test_name": "has_tag",
      "write": "nearby"
    }
  ],
  "label": "unknown",
  "tags": null,
  "task": "has_picked"
},
{
  "conditions": [
    {
      "arg": "TARGET",
      "label": "unknown",
      "query_name": "self",
      "test_name": "true",
      "write": "TARGET"
    }
  ],
  "label": "unknown",
  "tags": null,
  "task": "have_picked"
```

Launch tasks:

```perl
sub has_picked {
	my $buffer = shift;
	print "He has picked something: ".JSON->new->encode($buffer)."\n";
	my $caller = $buffer->{'caller'};
	my @message = ();
	my @nearby = @{ $buffer->{'nearby'} };
	foreach  my $n (0 .. (@nearby - 1)) {
		my $string = resolveWord($caller)." ".resolveWord("HAS_PICKED")." ".resolveWord($buffer->{'picked'}[0], "general");
		push(@message, $string);
		raise_flag($nearby[$n], "receives_message", {"message" => dclone(\@message)}, "has_picked");
	}
	return 1;
}

sub have_picked {
	my $buffer = shift;
	print "You picked something: ".JSON->new->encode($buffer)."\n";
	my $caller = $buffer->{'caller'};
	my @message = ();
	my $string = resolveWord("YOU_HAVE_PICKED")." ".resolveWord($buffer->{'picked'}[0], "general");
	push(@message, $string);
	raise_flag($caller, "receives_message", {"message" => dclone(\@message)}, "have_picked");
	return 1;
}

```

The next flag is the same as in the previous example.

### Build

Command script runs as:

```perl
my $class = $inbuf->{'arguments'}[0]->{'string'};
my $avatar = getAvatar($player);
my $buffer = {
	"caller" => $avatar
};
my $result = resolve_triggers_by_task("build", $class, $buffer);
```

Some class picks the trigger (in this example, ``stone_axe``):

```json
{
  "conditions": [
    {
      "amount": 1,
      "arg": "tree_branch",
      "label": "unknown",
      "query_name": "all_available",
      "tags": [],
      "test_name": "has_own_class",
      "write": "materials"
    },
    {
      "amount": 1,
      "arg": "stone_chip",
      "label": "unknown",
      "query_name": "all_available",
      "tags": [],
      "test_name": "has_own_class",
      "write": "materials"
    }
  ],
  "label": "unknown",
  "tags": null,
  "task": "build"
}
```

Which means:

```
do build if:
  all_available(has_own_class(tree_branch)) [1] as materials
and:
  all_available(has_own_class(stone_chip)) [1] as materials
```

If there is at least one stone chip and one tree branch around, run the task:

```perl
sub build {
	my $buffer = shift;
	my @targets = @{ $buffer->{'target'} };
	my @materials = @{ $buffer->{'materials'} };
	my @created_ids = ();
	foreach my $t (0 .. (@targets - 1)) {
		my $created_id = createNewObject($buffer->{'caller'}, $targets[$t]);
		push(@created_ids, $created_id) if ($created_id);
	}
	foreach my $m (0 .. (@materials - 1)) {
		destroyObject($materials[$m]);
	}
	raise_flag($buffer->{'caller'}, "have_made", { "made" => dclone(\@created_ids) }, "build");
	raise_flag($buffer->{'caller'}, "has_made", { "made" => dclone(\@created_ids) }, "build");
	return 1;
}
```

These flags are picked by console meta-class:

```json
{
  "conditions": [
    {
      "amount": "all",
      "arg": "it_looks",
      "label": "unknown",
      "query_name": "all_in_parent",
      "test_name": "has_tag",
      "write": "nearby"
    }
  ],
  "label": "unknown",
  "tags": null,
  "task": "has_made"
},
{
  "conditions": [
    {
      "arg": "TARGET",
      "label": "unknown",
      "query_name": "self",
      "test_name": "true",
      "write": "TARGET"
    }
  ],
  "label": "unknown",
  "tags": null,
  "task": "have_made"
}
```

This is almost exactly the same as the ``has|have_picked`` flags already discussed.

## Update

### Climate

Climate ``update`` hook triggers the updates:

```perl
for my $a (0 .. (@active_objects - 1)) {
	my $object = $active_objects[$a];
	if (has_tag($object, "listens_climate_update")) {
		update_climate($object);
	}
}
```

Note that this trigger is enclosed in a tag instead of processing all triggers.

Element atmosphere picks the trigger:

```json
  "tags": [
    "listens_climate_update"
  ]
```

The update code:

```perl
sub update_climate {
	my $id = shift;
	# ...more code here...
	my $min_height = -90.0 + $latitude + $declination;
	if ($max_height > 90.0) { $max_height = 180 - $max_height; }
	if ($min_height < -90.0) {	$min_height = -180 - $min_height; }
	my $sun_offset = ($min_height + $max_height)/2.0;
	my $sun_height = $sun_offset + ($max_height - $sun_offset)*sin(2*$PI*$time);
	modify_attribute($id, "sun_height", "set", $sun_height);
}
```

``modify_atribute`` takes care of raising the flags:

```perl
sub modify_attribute {
	my $id = shift;
	# ...more code here...
	if ($mvmt eq -1) {
		foreach my $cr (0 .. (@crossed - 1)) {
			my $rc = @crossed - 1 - $cr;
			my @lbl = ($crossed[$rc]);
			my @dir = ("down");
			my $obj = {
				"label" => dclone(\@lbl),
				"direction" => dclone(\@dir)
			};
			raise_flag($id, "listens_".$attribute, $obj, "modify_attribute");
		}
	} elsif ($mvmt eq 1) {
		foreach my $cr (0 .. (@crossed - 1)) {
			my @lbl = ($crossed[$cr]);
			my @dir = ("up");
			my $obj = {
				"label" => dclone(\@lbl),
				"direction" => dclone(\@dir)
			};
			raise_flag($id, "listens_".$attribute, $obj, "modify_attribute");
		}
	}
	return $result;
}
```

``listens_sun_height`` flag is now picked by atmosphere element:

```json
{
	"conditions": [{ "query_name": "all_in_self", "test_name": "has_tag", "arg": "it_looks", "write": "watches" }],
	"task": "listens_sun_height"
}
```

Which means:

```
do listens_sun_height if:
  all_in_self(has_tag(it_looks)) [] as watches
```

Console class picks the trigger:

```json
"tags": [
  "meta",
  "it_looks"
],
```

The flag runs as:

```perl
sub listens_sun_height {
	my $buffer = shift;
	my $caller = $buffer->{'caller'};
	my @watches = @{ $buffer->{'watches'} };
	my $sun_height = estimate_attribute($caller, "sun_height");
	my @message = ("\@sun_height:".int($sun_height)." \@MOVES:".$buffer->{'direction'}[0]." \@CROSSES:".$buffer->{'label'}[0]);
	for my $w (0 .. (@watches - 1)) {
		raise_flag($watches[$w], "receives_message", {"message" => dclone(\@message)}, "listens_sun_height");
	}
	return 1;
}
```

Message flag is the same as above.

# Spawns

## Draft

- Prepare a dedicated slot for the population in relevant classes (biomes). _5'_
- Prepare the json file for the population based on climate model (for spawns). _5'_
- Prepare the json file for the individual classes. _15'_
- Implement some cosmetic flags to test the behaviour of the spawned things. _15'_
- Implement a special attribute (redundant link) to indicate instance category:
spawned/volatile, permanent, unique, and perhaps registered (for players). _10'_
- Implement special condition types, e.g. to return a fixed value. _20'_
- Rewrite ``createNewObject()`` to make it more flexible or write similar functions. _20'_
- Implement callbacks ``spawn()`` and ``recall()`` similar to ``build()`` and
``destroy()``, that should also maintain the ``stock`` attribute. _25'_
- Implement ``init_*()`` for the relevant classes; use class name (and whatever
string is available) as tags for the function name.
Consider using triggers to automate some procedures but keep callback-based
design to retain control of the details. _25'_
- Fix the slot assignment procedure (bug that miscalculates free space). _25'_

_Estimated time: 2h30'--3h._
