sub self {
	my @array = ();
	push(@array, shift);
	return dclone(\@array);
}

sub parent {
	my @array = ();
	push(@array, getParentId(shift));
	return dclone(\@array);
}

sub all_in_parent {
	my $self = shift;
	my @array = @{ getAllInParent($self) };
	@array = grep !/^$self$/, @array;
	return dclone(\@array);
}

sub all_in_self {
	my @array = @{ getAllInSelf(shift) };
	return dclone(\@array);
}

sub all_in_self_recursive {
	my @array = @{ getAllInSelfRecursive(shift) };
	return dclone(\@array);
}

sub all_available {
	my $self = shift;
	my @array = ();
	push(@array, @{ getAllInParent($self) });			 # This is all fake, but hey
	push(@array, @{ getAllInSelfRecursive($self) });
	@array = grep !/^$self$/, @array;
	my @unique = uniq(@array);
	return dclone(\@unique);
}

sub all_classes {
	my @array = @{ dbQuerySingle("select id from everything where type = 'class';") };
	return dclone(\@array);
}

1;
