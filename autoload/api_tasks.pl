# MAIN DELIVER FUNCTION

sub test_deliver {
	# Read input
	local $/;
	my $json = shift;
	my $input = JSON::decode_json($json);

	# Sanitize input
	my $message_class = $input->{'message_class'};
	$message_class = 'plain' unless ($message_class);
	my $message_label = $input->{'message_label'};
	$message_label = 'EMPTY_LABEL' unless ($message_label);
	$input->{'message_class'} = $message_class;
	$input->{'message_label'} = $message_label;

	# Try the message class
	my $formatted;
	eval "\$formatted = message_class_".$message_class."(\$input);";
	$formatted = message_class_exception($input) unless ($formatted);
  
  # Identify the recipient
	my $recipient = $formatted->{'player'};
  $recipient = getAvatarPlayer($formatted->{'recipient'}) unless ($recipient);
  $recipient = $INPUT->{'player'} unless ($recipient);
  
  # Create array and parse colors
	my @messages = @{ $formatted->{'messages'} };
  for (@messages) {
    s/COL0/$COL0/g,
    s/COL1/$COL1/g,
    s/COL2/$COL2/g,
    s/COL3/$COL3/g,
    s/COL4/$COL4/g
  }

  # Send API call
	callSend( $recipient, 'plain', dclone(\@messages) ) if ( isPlayerConnected($recipient) );

  1;
}

# This call was used for testing, no idea if we use it now
sub look {
	my $json = shift;
	my $input = JSON::decode_json($json);
	$input->{'recipient'} = $input->{'caller'};
	if (@{ $input->{'parent'} }) {
		my $temp = $input;
		$temp->{'message_label'} = 'you_are_in';
		test_deliver(JSON->new->encode($temp));
	}
	if (@{ $input->{'nearby'} }) {
		my $temp = $input;
		$temp->{'message_label'} = 'you_see_nearby';
		test_deliver(JSON->new->encode($temp));
	}
}

1;
