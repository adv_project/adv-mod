sub look_around {
	my $buffer = shift;
	my $self = $buffer->{'caller'};
	my $player = $INPUT->{'player'};
	my $parent = $buffer->{'parent'}[0];
#	my $slot = getParentSlot($self);
	my @message = ();
	
	@message = ("\@YOU_ARE_IN:".$parent);
	raise_flag($self, "receives_message", {"message" => dclone(\@message)}, "SCRIPT:look");
	
	my $sun_height = estimate_attribute($parent, "sun_height");
	@message = ("\@sun_height:".int($sun_height));
	raise_flag($self, "receives_message", {"message" => dclone(\@message)}, "SCRIPT:look");
	
	if (@{ $buffer->{'nearby'} }) {
		my $nearby = join(',', @{ $buffer->{'nearby'} });
		@message = ("\@YOU_SEE_NEARBY:".$nearby);
		raise_flag($self, "receives_message", {"message" => dclone(\@message)}, "SCRIPT:look");
	}
}

sub move_class {
	my $buffer = shift;
	my $target = $buffer->{'target'}[0];
	my $avatar = $buffer->{'caller'};
	
	my $try = prepareParentLink($avatar, $target);
	if ($try->{'result'} eq "success") {
		removeParentLink($avatar);
		createParentLink($avatar, $try->{'id'}, $try->{'slot'}, 0);
#		raise_flag($avatar, "looks_around", {}, "move_class");
	} else {
		# BAD: this should be a flag
		callSend(getAvatarPlayer($avatar), "plain", "There is no way there.");
	}
	return 1;
}

sub build {
	my $buffer = shift;
	my @targets = @{ $buffer->{'target'} };
	my @materials = @{ $buffer->{'materials'} };
	my @created_ids = ();
	foreach my $t (0 .. (@targets - 1)) {
		my $created_id = createNewObject($buffer->{'caller'}, $targets[$t]);
		push(@created_ids, $created_id) if ($created_id);
	}
	foreach my $m (0 .. (@materials - 1)) {
		destroyObject($materials[$m]);
	}
	# This should be done by raising a flag inside createNewObject()
	raise_flag($buffer->{'caller'}, "have_made", { "made" => dclone(\@created_ids) }, "build");
	raise_flag($buffer->{'caller'}, "has_made", { "made" => dclone(\@created_ids) }, "build");
#	raise_flag($buffer->{'caller'}, "makes_noise", {});
#	raise_flag($buffer->{'caller'}, "makes_physical_work", {});
	return 1;
}

sub pick {
	my $buffer = shift;
	my @targets = @{ $buffer->{'target'} };
	my $newparent = $buffer->{'caller'};
	foreach my $t (0 .. (@targets - 1)) {
		my $try = prepareParentLink($targets[$t], $newparent);
		if ($try->{'result'} eq "success") {
			removeParentLink($targets[$t]);
			createParentLink($targets[$t], $try->{'id'}, $try->{'slot'}, 0);
		}
	}
	raise_flag($buffer->{'caller'}, "have_picked", { "picked" => dclone(\@targets) }, "pick");
	raise_flag($buffer->{'caller'}, "has_picked", { "picked" => dclone(\@targets) }, "pick");
#	raise_flag($buffer->{'caller'}, "makes_physical_work", {});
#	raise_flag($buffer->{'caller'}, "checks_his_weight", {});
}

sub drop {
	my $buffer = shift;
#	print "[api_tasks/drop()] Dropping something: ".JSON->new->encode($buffer)."\n";
	my @targets = @{ $buffer->{'target'} };
	my $newparent = getParentId($buffer->{'caller'});
	foreach my $t (0 .. (@targets - 1)) {
		my $try = prepareParentLink($targets[$t], $newparent);
		if ($try->{'result'} eq "success") {
			removeParentLink($targets[$t]);
			createParentLink($targets[$t], $try->{'id'}, $try->{'slot'}, 0);
		}
	}
	raise_flag($buffer->{'caller'}, "have_dropped", { "dropped" => dclone(\@targets) }, "drop");
	raise_flag($buffer->{'caller'}, "has_dropped", { "dropped" => dclone(\@targets) }, "drop");
#	raise_flag($buffer->{'caller'}, "checks_his_weight", {});
}

#TODO: move this function to some other file
sub getRandomInteger {
	my $min = shift;
	my $max = shift;
	my $range = $max - $min;
	my $random_value = rand($range) + 1;
	my $result = int($random_value + $min);
	return $result;
}

sub getWinner {
	my $dice_pool = shift;
	my $dice = shift;
	my $canvas = shift;
	my @ordered = ();
}

sub spawn {
	my $buffer = shift;
	logAppend(6, 'Spawn: '.JSON->new->encode($buffer));
	my $caller = $buffer->{'caller'};
	my $parent = $buffer->{'parent'}[0];
	my @classes = @{ $buffer->{'classes'} };
	my $dice_pool = int(0);
	my $canvas = {};
	#TODO: this should be a function, getWinnerOrSomething()
	for my $c (0 .. (@classes - 1)) {
		my $p = estimate_attribute($classes[$c], "spawn_probability");
		$dice_pool = $dice_pool + $p;
		$canvas->{"".$p.""} = $classes[$c];
	}
	my $iterations = estimate_attribute($caller, "spawn_iterations");
	my $success_chance = estimate_attribute($caller, "spawn_chance");
	for my $iter (1 .. $iterations) {
		print "Spawning something (perhaps...)\n";
		my $dice = getRandomInteger(0, $dice_pool);
		my $success = getRandomInteger(0, 100);
		logAppend(7, "Success says ".$success."");
		next unless ($success <= $success_chance);
		logAppend(7, "Dice says ".$dice."");
		my $winner;
		foreach my $k (keys(%{ $canvas })) {
			my $bet = int($k);
			if ($bet <= $dice) {
				$winner = $canvas->{$k};
				last;
			}
		}
		next unless ($winner);
		my $created_id = spawnNewObject($parent, $winner);
		modify_attribute($caller, "population_stock", "delta", -1);
		print "Spawning a $winner\n";
		# What should we do with this id, that couldn't be done by some flag raised elsewhere?
	}
}

sub recall {
	my $buffer = shift;
	logAppend(6, 'Recall: '.JSON->new->encode($buffer));
	print 'Recall: '.JSON->new->encode($buffer)."\n";
	my $caller = $buffer->{'caller'};
	my $parent = $buffer->{'parent'}[0];
	my @recalled = @{ $buffer->{'recalled'} };
	for my $r (0 .. (@recalled - 1)) {
		my $id = $recalled[$r];
		destroyObject($id);
	}
}

1;
