# Color constants
$RESET = "\0110";
$BOLD = "\0112";
$ITALIC = "\0113";
$UNDERLINE = "\0114";
$NOTBOLD = "\0115";
$NOTITALIC = "\0116";
$NOTUNDERLINE = "\0117";
$RED = "\011r";
$GREEN = "\011g";
$YELLOW = "\011y";
$BLUE = "\011b";
$MAGENTA = "\011m";
$CYAN = "\011c";
$WHITE = "\011w";
$POSITIVE = "\011p";
$NEGATIVE = "\011n";

# Gonfiguration options (configurable per player)
$COL0 = $RESET;
$COL1 = $BOLD.$WHITE;
$COL2 = $BOLD.$GREEN;
$COL3 = $BOLD.$YELLOW;
$COL4 = $BOLD.$MAGENTA;
$ADV_LANG = 'en';

1;
