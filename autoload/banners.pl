#!/usr/bin/env perl

#
# Banners
#

sub bannerCreateWorld {
	my $player = shift;
		my @greetings = (
		$YELLOW.$BOLD.'World creation sequence'.$RESET,
			'  Use '.$GREEN.'show'.$RESET.' to view current world parameters, '.$GREEN.'set'.$RESET.' to change',
			'  them and '.$GREEN.'ok'.$RESET.' to generate the world. See '.$GREEN.'help'.$RESET.' for details.'
		);
		callSend($player, 'plain', dclone(\@greetings));
}

sub bannerCreateCharacter {
	my $player = shift;
	my @greetings = (
	$YELLOW.$BOLD.'Character creation sequence'.$RESET,
		'  Use '.$GREEN.'show'.$RESET.' to view your current attributes, '.$GREEN.'set'.$RESET.' to change',
		'  them and '.$GREEN.'ok'.$RESET.' to throw yourself in. See '.$GREEN.'help'.$RESET.' for details.'
	);
	callSend($player, 'plain', dclone(\@greetings));
}

sub bannerEnterGame {
	my $player = shift;
		my @greetings = (
		$YELLOW.$BOLD."Let's play!".$RESET,
			'  Use '.$GREEN.'help'.$RESET.' to get some hints, and '.$GREEN.'help '.$BLUE.'<command>'.$RESET.' for',
			'  details.'
		);
		callSend($player, 'plain', dclone(\@greetings));
}

1;
