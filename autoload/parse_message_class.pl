# MESSAGE CLASSES
#
# Called as such from the api calls stack (defined in the json buffer)

sub message_class_plain {
	my $input = shift;
	my $recipient = $input->{'recipient'};
  my $lang;
	if ($recipient) {
    $lang = dbQueryAtom("select language from players where avatar = '".$recipient."';");
  } else {
    $lang = dbQueryAtom("select language from players where username = '".$INPUT->{'player'}."';");
  }
	my $label = $input->{'message_label'};
  my $format = dbQueryAtom("select sentence from dict_labels where language = '".$lang."' and identifier = '".$label."';");
	$format = '['.$label.']' unless ($format);
	my @messages = @{ parse_format($format, $input) };
	$input->{'messages'} = dclone(\@messages);
	return $input;
}

sub message_class_private {
	my $input = shift;
	my $recipient = $input->{'recipient'};
  my $lang;
	if ($recipient) {
    $lang = dbQueryAtom("select language from players where avatar = '".$recipient."';");
	} elsif ($input->{'player'}) {
    $lang = dbQueryAtom("select language from players where avatar = '".$input->{'player'}."';");
  } else {
    $lang = dbQueryAtom("select language from players where username = '".$INPUT->{'player'}."';");
  }
	my $label = $input->{'message_label'};
  my $format = dbQueryAtom("select sentence from dict_labels where language = '".$lang."' and identifier = '".$label."';");
	$format = $label unless ($format);
	my @messages = @{ parse_format($format, $input) };
	$input->{'messages'} = dclone(\@messages);
	return $input;
}

sub message_class_exception {
	my $input = shift;
	my @messages = ($COL4.'Unable to parse message class: '.JSON->new->encode($input));
	$input->{'messages'} = dclone(\@messages);
	return $input;
}

sub message_class_error {
	my $input = shift;
	my $recipient = $input->{'recipient'};
  my $lang;
	if ($recipient) {
    $lang = dbQueryAtom("select language from players where avatar = '".$recipient."';");
	} elsif ($input->{'player'}) {
    $lang = dbQueryAtom("select language from players where avatar = '".$input->{'player'}."';");
  } else {
    $lang = dbQueryAtom("select language from players where username = '".$INPUT->{'player'}."';");
  }
	my $label = $input->{'message_label'};
  my $string = dbQueryAtom("select sentence from dict_labels where language = '".$lang."' and identifier = '".$label."';");
	$string = $label unless ($string);
	my @messages = ($COL4.$string);
	$input->{'messages'} = dclone(\@messages);
	return $input;
}

sub message_class_help {
	my $input = shift;
	my $recipient = $input->{'recipient'};
  my $lang;
  my $game_phase;
	if ($recipient) {
    $lang = dbQueryAtom("select language from players where avatar = '".$recipient."';");
    $game_phase = dbQueryAtom("select game_phase from players where avatar = '".$recipient."';");
  } else {
    $lang = dbQueryAtom("select language from players where username = '".$INPUT->{'player'}."';");
    $game_phase = dbQueryAtom("select game_phase from players where username = '".$INPUT->{'player'}."';");
  }
	my $label = $input->{'message_label'};
	$label = "help_index" unless ($label);
  my @messages = @{ dbQuerySingle("select unnest(message) from dict_help where language = '".$lang."' and identifier = '".$label."' and game_phase = '".$game_phase."';") };
  if (!@messages) {
    my $exception_label = 'no_help_available';
    @messages = @{ dbQuerySingle("
      select unnest(message) from dict_help
      where language = '".$lang."'
      and identifier = '".$exception_label."';"
    ) };
    @messages = ($exception_label) unless (@messages);
  }
	$input->{'messages'} = dclone(\@messages);
	return $input;
}

sub message_class_menu {
	my $input = shift;
  my $lang = dbQueryAtom("select language from players where username = '".$input->{'player'}."';");
	my $label = $input->{'message_label'};
  print "Yes, indeed, sire: ".JSON->new->encode($input)."\n";
  my @messages = (JSON->new->encode($input->{'candidates'}));
	$input->{'messages'} = dclone(\@messages);
	return $input;
}

1;
