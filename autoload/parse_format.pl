# THE LITTLE BRAIN

sub parse_format {
	my $format = shift;
	my $input = shift;
	my @words = split(/%/, $format);
	my $string;
  my @messages;
  # Add a conditional here -- TODO: define label for multiline format
  if ($format =~ /^_multiline_/) {
    # Multiline
  	foreach my $w (0 .. (@words - 1)) {
      if ($words[$w] =~ /^_multiline_/) {
        next;
  		} elsif ($words[$w] =~ /^?@/) {
  			my @args = split(/@/, $words[$w]);
  			my $call = shift(@args);
  			my @parsed;
  			eval "\@parsed = \@{ parse_".$call."(\$input, \@args) };";
  			push(@messages, @parsed);
  		} else {
  			push(@messages, $words[$w]);
  		}
  	}
  } else {
    # Single line
  	foreach my $w (0 .. (@words - 1)) {
  		if ($words[$w] =~ /^?@/) {
  			my @args = split(/@/, $words[$w]);
  			my $call = shift(@args);
  			my $parsed;
  			eval "\$parsed = parse_".$call."(\$input, \@args);";
  			$string = $string.$parsed;
  		} else {
  			$string = $string.$words[$w];
  		}
  	}
    @messages = ($string);
  }
  @messages = ('nothing_relevant') unless (@messages);
	return dclone(\@messages);
}

1;
