sub run_api_script {
	# Read input
	local $/;
	my $json = shift;
	my $input = JSON::decode_json($json);

	# Gather input
  my $relpath = $input->{'path'};
  my $path = $WDIR."/".$relpath;
  $input->{'player'} = $INPUT->{'player'} unless ($input->{'player'});
  callRun($path, $input);
}

sub receives_message {
	my $buffer = shift;
#	print "He receives a message: ".JSON->new->encode($buffer)."\n";
	my $caller = $buffer->{'caller'};
	my $player = getAvatarPlayer($caller);
	my @messages = @{ $buffer->{'message'} };
	foreach my $m (0 .. (@messages - 1)) {
		callSend($player, "plain", $buffer->{'message'}[$m])
			unless (! isPlayerConnected($player));
	}
	return 1;
}

sub has_made {
	my $buffer = shift;
	print "He has made something: ".JSON->new->encode($buffer)."\n";
	my $caller = $buffer->{'caller'};
	my @message = ();
	my @nearby = @{ $buffer->{'nearby'} };
	foreach  my $n (0 .. (@nearby - 1)) {
		my $string = resolveWord($caller)." ".resolveWord("HAS_MADE")." ".resolveWord($buffer->{'made'}[0], "general");
		push(@message, $string);
		raise_flag($nearby[$n], "receives_message", {"message" => dclone(\@message)}, "has_made");
	}
	return 1;
}

sub have_made {
	my $buffer = shift;
	print "You have made something: ".JSON->new->encode($buffer)."\n";
	my $caller = $buffer->{'caller'};
	my @message = ();
	my $string = resolveWord("YOU_HAVE_MADE")." ".resolveWord($buffer->{'made'}[0], "general");
	push(@message, $string);
	raise_flag($caller, "receives_message", {"message" => dclone(\@message)}, "have_made");
	return 1;
}

sub has_picked {
	my $buffer = shift;
	print "He has picked something: ".JSON->new->encode($buffer)."\n";
	my $caller = $buffer->{'caller'};
	my @message = ();
	my @nearby = @{ $buffer->{'nearby'} };
	foreach  my $n (0 .. (@nearby - 1)) {
		my $string = resolveWord($caller)." ".resolveWord("HAS_PICKED")." ".resolveWord($buffer->{'picked'}[0], "general");
		push(@message, $string);
		raise_flag($nearby[$n], "receives_message", {"message" => dclone(\@message)}, "has_picked");
	}
	return 1;
}

sub have_picked {
	my $buffer = shift;
	print "You picked something: ".JSON->new->encode($buffer)."\n";
	my $caller = $buffer->{'caller'};
	my @message = ();
	my $string = resolveWord("YOU_HAVE_PICKED")." ".resolveWord($buffer->{'picked'}[0], "general");
	push(@message, $string);
	raise_flag($caller, "receives_message", {"message" => dclone(\@message)}, "have_picked");
	return 1;
}

sub has_dropped {
	my $buffer = shift;
	print "He has dropped something: ".JSON->new->encode($buffer)."\n";
	my $caller = $buffer->{'caller'};
	my @message = ();
	my @nearby = @{ $buffer->{'nearby'} };
	foreach  my $n (0 .. (@nearby - 1)) {
		my $string = resolveWord($caller)." ".resolveWord("HAS_DROPPED")." ".resolveWord($buffer->{'dropped'}[0], "general");
		push(@message, $string);
		raise_flag($nearby[$n], "receives_message", {"message" => dclone(\@message)}, "has_dropped");
	}
	return 1;
}

sub have_dropped {
	my $buffer = shift;
	print "He has dropped something: ".JSON->new->encode($buffer)."\n";
	my $caller = $buffer->{'caller'};
	my @message = ();
	my $string = resolveWord("YOU_HAVE_DROPPED")." ".resolveWord($buffer->{'dropped'}[0], "general");
	push(@message, $string);
	raise_flag($caller, "receives_message", {"message" => dclone(\@message)}, "have_dropped");
	return 1;
}

sub looks_around {
	my $caller = shift;
	my $buffer = shift;
	my $player = getAvatarPlayer($caller);
	callSend($player, "plain", resolveWord("YOU_SHOULD_BE_LOOKING_AROUND_NOW"));
	return 1;
}

sub makes_physical_work {
	return 1;
}

sub makes_noise {
	return 1;
}

sub checks_his_weight {
	return 1;
}

sub listens_altitude {
	my $buffer = shift;
	print "He senses the altitude: ".JSON->new->encode($buffer)."\n";
	return 1;
}

sub listens_temperature {
	my $buffer = shift;
	print "He senses the temperature: ".JSON->new->encode($buffer)."\n";
	return 1;
}

1;
