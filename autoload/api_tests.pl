sub true {
	return 1;
}

sub is_connected {
	my $self = shift;
	my $player = getAvatarPlayer($self);
	return 0 unless ($player);
	return 1 if ( dbQueryAtom("select count(username) from players where username = '".$player."' and connected = true;") );
	return 0;
}

sub has_tag {
	my $self = shift;
	my $tag = shift;
	return hasTag($self, $tag);
}

sub has_class {
	my $self = shift;
	my $class = shift;
	return hasClass($self, $class);
}

sub has_own_class {
	my $self = shift;
	my $class = shift;
	return hasOwnClass($self, $class);
}

sub has_attribute_lt {
	my $self = shift;
	my $attribute = shift;
	my $value = shift;
	my $our_value = estimate_attribute($self, $attribute);
	logAppend(7, "...HAS_ATTRIBUTE_LT... ".$self."->".$attribute.".: ".$value." vs ".$our_value);
	if($our_value < $value) {
		return 1;
	} else {
		return 0;
	}
}

sub has_attribute_ge {
	my $self = shift;
	my $attribute = shift;
	my $value = shift;
	my $our_value = estimate_attribute($self, $attribute);
	logAppend(7, "...HAS_ATTRIBUTE_GE... ".$self."->".$attribute.".: ".$value." vs ".$our_value);
	if($our_value >= $value) {
		return 1;
	} else {
		return 0;
	}
}

1;
