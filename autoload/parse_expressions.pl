# PARSE EXPRESSION
#
# Each 'parse_EXP' function returns the string that should replace the %EXP:ARG%
# token when parsing db output

# %N% is replaced by the recipient name
sub parse_N {
  my $input = shift;
  my $recipient_name = dbQueryAtom("select name from players where avatar = '".$input->{'recipient'}."';");
  return $recipient_name;
}

# %p@arg% concatenates recursive parents
sub parse_p {
  my $input = shift;                      # Read input
  my $recipient = $input->{'recipient'};  # Identify recipient

  # Guess which player sent the call
  my $caller;
  if ($recipient) {
    # Input has avatar, guess accordingly
    $caller = dbQueryAtom("select username from players where avatar = '".$recipient."';");
  } elsif ($recipient) {
    # Input has player, take it
    $caller = $input->{'player'};
  } else {
    # Input does not specify recipient, send to global player
    $caller = $INPUT->{'player'};
  }

  my $comma = resolve_word($caller, 'comma', 'atom');
  my $in = resolve_word($caller, 'in', 'atom');
  my @known_classes = ();
  my $output; 
 
 # Cycle through the input arrays pointed to in @args (e.g. the 'subject' in %p@subject%)
 while (1) {
    my $arg = shift;
    last unless ($arg);
    my @array = @{ $input->{$arg} };      # Accessing buffer->arg as returned by the conditions

    # Cycle through the buffer array
    foreach my $a (0 .. (@array - 1)) {
      #TODO: replace this query with true knowledge, plus given name and other special cases
      #        for now we use the first class right away
      my $known_class = resolve_known_class($caller, $array[$a]);
      push(@known_classes, $known_class);
    } # foreach buffer->array[a]
  } # while ($arg)

  foreach my $k (0 .. (@known_classes - 1)) {
    my $token = $known_classes[$k];
    my $prefix;
    my $selected_word;
    
    # Resolve separator: and, comma, or nothing depending of count and position
    if ($k ne 0) {
      $prefix = $comma.' '.$in.' ';
    } else {
      $prefix = $in.' ';
    }

    # Build output
    my $selected_word = resolve_word($caller, $token, 'one');
    $selected_word = '['.$token.']' unless ($selected_word);
    $output = $output.$prefix.$selected_word;
  }
  return $output;
}

# %e@arg% concatenates the short descriptions for each input->arg array element,
# resolving groups of objects and knowledge
sub parse_e {
  my $input = shift;                      # Read input
  my $recipient = $input->{'recipient'};  # Identify recipient

  # Guess which player sent the call
  my $caller;
  if ($recipient) {
    # Input has avatar, guess accordingly
    $caller = dbQueryAtom("select username from players where avatar = '".$recipient."';");
  } elsif ($recipient) {
    # Input has player, take it
    $caller = $input->{'player'};
  } else {
    # Input does not specify recipient, send to global player
    $caller = $INPUT->{'player'};
  }

  my $and = resolve_word($caller, 'and', 'atom');
  my $comma = resolve_word($caller, 'comma', 'atom');
  my $grouped = {};   # This is the output message structure
 
 # Cycle through the input arrays pointed to in @args (e.g. the 'subject' in %e@subject%)
 while (1) {
    my $arg = shift;
    last unless ($arg);
    my @array = @{ $input->{$arg} };      # Accessing buffer->arg as returned by the conditions

    # Cycle through the buffer array
    foreach my $a (0 .. (@array - 1)) {
      #TODO: replace this query with true knowledge, plus given name and other special cases
      #        for now we use the first class right away
      my $known_class = resolve_known_class($caller, $array[$a]);
      # This updates the count of things in groups
      if ($grouped->{$known_class}) {
        $grouped->{$known_class} = $grouped->{$known_class} + 1;
      } else {
        $grouped->{$known_class} = 1;
      }
    } # foreach buffer->array[a]
  } # while ($arg)

  my $output; 
  my @groups;

  # Build up an array with the groups obtained
  foreach my $k (keys(%{$grouped})) {
    push(@groups, $k);
  }

  foreach my $k (0 .. (@groups - 1)) {
    my $count = $grouped->{$groups[$k]};
    my $token = $groups[$k];
    my $prefix;
    my $selected_word;
    
    # Resolve separator: and, comma, or nothing depending of count and position
    if ($k eq $#groups and $k ne 0) {
      $prefix = ' '.$and.' ';
    } elsif ($k gt 0) {
      $prefix = $comma.' ';
    }

    # Build output
    if ($count eq 1) {
      my $selected_word = resolve_word($caller, $token, 'one');
      $selected_word = '['.$token.']' unless ($selected_word);
      $output = $output.$prefix.$selected_word;
    } elsif ($count gt 1) {
      my $selected_word = resolve_word($caller, $token, 'many');
      $selected_word = '% ['.$token.']' unless ($selected_word);
      $selected_word =~ s/%/$count/;
      $output = $output.$prefix.$selected_word;
    }
  }
  return $output;
}

# Inventory (shows location for every object)
sub parse_I {
  my $input = shift;
  my $recipient = $input->{'recipient'};

  # Guess which player sent the call
  my $caller;
  if ($recipient) {
    # Input has avatar, guess accordingly
    $caller = dbQueryAtom("select username from players where avatar = '".$recipient."';");
  } elsif ($recipient) {
    # Input has player, take it
    $caller = $input->{'player'};
  } else {
    # Input does not specify recipient, send to global player
    $caller = $INPUT->{'player'};
  }

  my $and = resolve_word($caller, 'and', 'atom');
  my $comma = resolve_word($caller, 'comma', 'atom');
  my $grouped = {};   # This is the output message structure
 
  while (1) {
    my $arg = shift;
    last unless ($arg);
    my @array = @{ $input->{$arg} };
    foreach my $a (0 .. (@array - 1)) {
      my $known_class = resolve_known_class($caller, $array[$a]);
      my $slot_name = dbQueryAtom("select name from links where id = '".$array[$a]."' and type = 'parent';");
      $slot_name = "undefined" unless ($slot_name);
      if ($grouped->{$slot_name}->{$known_class}) {
        $grouped->{$slot_name}->{$known_class} = $grouped->{$slot_name}->{$known_class} + 1;
      } else {
        $grouped->{$slot_name}->{$known_class} = 1;
      }
    }
  }

  my $string;

  my @slotkeys = ();
  foreach my $sk (keys(%{ $grouped })) {
    push(@slotkeys, $sk);
  }
  my $cnt = 0;
  my $skeys = $#slotkeys+1;

  foreach my $s (keys(%{$grouped})) {
    $cnt = $cnt + 1;
    my $ssuffix;
    if (($skeys > 1) and ($cnt == $skeys-1)) {
      $ssuffix = ', and ';
    } elsif (($skeys > 1) and ($cnt < $skeys-1)) {
      $ssuffix = ', ';
    }

    my @keys = ();
    foreach my $k (keys(%{ $grouped->{$s} })) {
      push(@keys, $k);
    }

    foreach my $k (0 .. (@keys - 1)) {
      my $n = $grouped->{$s}->{$keys[$k]};
      my $l = $keys[$k];
      my $prefix;
      
      if ($k eq $#keys and $k ne 0) {
        $prefix = ' '.$and.' ';
      } elsif ($k gt 0) {
        $prefix = $comma.' ';
      }
  
      if ($n eq 1) {
  #     TODO: use player language instead of global
        my $f = resolve_word($caller, $l, 'one');
        $f = '['.$l.']' unless ($f);
        $string = $string.$prefix.$f;
      } elsif ($n gt 1) {
        my $f = resolve_word($caller, $l, 'many');
        $f = '% ['.$l.']' unless ($f);
        $f =~ s/%/$n/;
        $string = $string.$prefix.$f;
      }
    }
    my $ss = resolve_word($caller, $s, 'atom');
    $ss = '['.$s.']' unless ($ss);
    $string = $string.' ('.$ss.')'.$ssuffix;
  }
  return $string;
}

# Analyze (shows complete stats for every object)
sub parse_A {
  my $input = shift;
  my $recipient = $input->{'recipient'};

  # Guess which player sent the call
  my $caller;
  if ($recipient) {
    # Input has avatar, guess accordingly
    $caller = dbQueryAtom("select username from players where avatar = '".$recipient."';");
  } elsif ($recipient) {
    # Input has player, take it
    $caller = $input->{'player'};
  } else {
    # Input does not specify recipient, send to global player
    $caller = $INPUT->{'player'};
  }

  my $and = resolve_word($caller, 'and', 'atom');
  my $comma = resolve_word($caller, 'comma', 'atom');
  my $grouped = {};   # This is the output message structure
  my @messages;   # Message lines returned
 
  while (1) {
    my $arg = shift;
    last unless ($arg);
    my @array = @{ $input->{$arg} };
    foreach my $a (0 .. (@array - 1)) {
      my $known_class = resolve_known_class($caller, $array[$a]);
      my $slot_name = dbQueryAtom(
        "select name from links
         where id = '".$array[$a]."'
         and type = 'parent';"
      );
      $slot_name = "undefined" unless ($slot_name);
      my $ss = resolve_word($caller, $slot_name, 'atom');
      $ss = '['.$s.']' unless ($ss);
      my $f = resolve_word($caller, $known_class, 'description_short');
      $f = '['.$known_class.']' unless ($f);
      my @c = @{ resolve_color($caller, $array[$a]) };
      @c = ('['.$known_class.']') unless (@c);
      my $integrity = dbQueryAtom("select estimate_attribute('".$array[$a]."', 'integrity');");
      push(@messages, $ss.': '.$f." (".join(', ', @c).") (".int($integrity)."%)");
      my @d = @{ resolve_description($caller, $known_class) };
      push(@messages, @d);
    }
  }

  return dclone(\@messages);
}

# Neighbouring tiles (assume parent neighbours)
sub parse_n {
  my $input = shift;
  my $recipient = $input->{'recipient'};

  # Guess which player sent the call
  my $caller;
  if ($recipient) {
    # Input has avatar, guess accordingly
    $caller = dbQueryAtom("select username from players where avatar = '".$recipient."';");
  } elsif ($recipient) {
    # Input has player, take it
    $caller = $input->{'player'};
  } else {
    # Input does not specify recipient, send to global player
    $caller = $INPUT->{'player'};
  }
  my $self = getAvatar($caller);  # Blunt, but makes sure we have something
  my $parent = dbQueryAtom("select get_parent_tile('".$self."');");

  my $and = resolve_word($caller, 'and', 'atom');
  my $comma = resolve_word($caller, 'comma', 'atom');
  my $to_the = resolve_word($caller, 'to_the', 'atom');
  my $there_is = resolve_word($caller, 'there_is', 'atom');
  my $grouped = {};   # This is the output message structure
  my @messages;   # Message lines returned
 
  while (1) {
    my $arg = shift;
    last unless ($arg);
    my @array = @{ $input->{$arg} };
    foreach my $a (0 .. (@array - 1)) {
      my $known_class = resolve_known_class($caller, $array[$a]);
      $known_class = $array[$a] unless ($known_class);
      my $link_name = dbQueryAtom(
        "select n.name from links n
         where n.id = '".$parent."'
         and n.type = 'neighbour'
         and n.target = '".$array[$a]."';"
      );
      $link_name = "undefined" unless ($link_name);
#      if ($grouped->{$known_class}) {
 #       my @a = ($link_name);
  #      $grouped->{$known_class} = dclone(\@a);
   #   } else {
        push(@{$grouped->{$known_class}}, $link_name);
    #  }
    }
#      my $ss = resolve_word($caller, $link_name, 'atom');
 ##     $ss = '['.$link_name.']' unless ($ss);
   #   my $f = resolve_word($caller, $known_class, 'one');
    #  $f = '['.$known_class.']' unless ($f);
  }

  my @distinct = ();
  for my $d (keys(%{$grouped})) {
    push(@distinct, $d);
  }
  my $cnt;
  for my $d (keys(%{$grouped})) {
    $cnt = 0+@{$grouped->{$d}};
    my $prefix;
    if (@distinct < 2) {
      $prefix = resolve_word($caller, 'in_all_directions', 'atom');
    } else {
      my $directions;
      for my $i (0 .. (@{$grouped->{$d}} - 1)) {
        my $word = resolve_word($caller, $grouped->{$d}[$i], 'atom');
        if ($i < 1) {
          $directions = $directions.$word;
        } elsif ($i < (@{$grouped->{$d}} - 1)) {
          $directions = $directions.$comma.' '.$word;
        } else {
          $directions = $directions.' '.$and.' '.$word;
        }
      }
      $prefix = resolve_word($caller, 'to_the', 'atom').' '.$directions;
    }
    my $string = $prefix.$comma.' '.$there_is.' '.resolve_word($caller, $d, 'atom');
    push(@messages, $string);
  }
  return dclone(\@messages);
#  return join(', ', @messages);
}

sub resolve_known_class {
  my $caller = shift; # The avatar id
  my $token = shift;  # e.g. 'stone_knife'
  my $known_token;
  my $avatar = dbQueryAtom("select avatar from players where username = '".$caller."';");
  if ($avatar) {
    # Check avatar knowledge
    $known_token = dbQueryAtom("select resolve_token('".$token."', '".$avatar."');");
    $known_token = 'null_token' unless ($known_token);;
  } else {
    # Use the token unchanged
    $known_token = $token;
  }
  return $known_token;
}

sub resolve_word {
  my $caller = shift; # The avatar id
  my $token = shift;  # e.g. 'stone_knife'
  my $type = shift;   # e.g. 'one', 'many'
  my $lang = dbQueryAtom("select language from players where username = '".$caller."';");
  my $word;
  $word = dbQueryAtom("
    select ".$type." from dict_tokens
    where language = '".$lang."'
    and identifier = '".$token."';
  ");
  $word = '['.$token.']' unless ($word);
  return $word;
}

sub resolve_description {
  my $caller = shift; # The avatar id
  my $token = shift;  # e.g. 'stone_knife'
  my $lang = dbQueryAtom("select language from players where username = '".$caller."';");
  my @messages = @{ dbQuerySingle("
    select unnest(description_long) from dict_tokens
    where language = '".$lang."'
    and identifier = '".$token."';
  ") };
  return dclone(\@messages);;
}

sub resolve_color {
  my $caller = shift; # The avatar id
  my $token = shift;  # e.g. 'stone_knife'
  my @colors = @{ dbQuerySingle("select distinct unnest(resolve_object_color('".$token."'));") };
  for (@colors) { s/^/color/; }
  my $lang = dbQueryAtom("select language from players where username = '".$caller."';");
  my @parsed;
  for my $c (0 .. (@colors - 1)) {
    my $col = dbQueryAtom("
    select atom from dict_tokens
    where language = '".$lang."'
    and identifier = '".$colors[$c]."';
  ");
    $col = $colors[$c] unless ($col);
    push(@parsed, $col);
  }
  return dclone(\@parsed);
}

1;
